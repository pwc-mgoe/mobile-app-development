import React, { useEffect, useRef } from "react";
import {
  DefaultTheme,
  Provider as PaperProvider,
  configureFonts,
} from "react-native-paper";
import Index from "./src/Index";
import { NavigationContainer } from "@react-navigation/native";
import AppLoading from "expo-app-loading";
import { useFonts } from "expo-font";
import * as Sentry from "sentry-expo";
import * as Analytics from "expo-firebase-analytics";

Sentry.init({
  dsn: "https://5ff912afc9fe400fa68d4831c1701958@o615389.ingest.sentry.io/5749913",
  enableInExpoDevelopment: true,
  debug: true, // Sentry will try to print out useful debugging information if something goes wrong with sending an event. Set this to `false` in production.
});

//Theme customisation
const fontConfig = {
  ios: {
    bold: {
      fontFamily: "AzoSans-Bold",
    },
    regular: {
      fontFamily: "AzoSans-Regular",
      fontWeight: "normal",
    },
    medium: {
      fontFamily: "AzoSans-Medium",
      fontWeight: "normal",
    },
    light: {
      fontFamily: "AzoSans-Light",
      fontWeight: "normal",
    },
    thin: {
      fontFamily: "AzoSans-Thin",
      fontWeight: "normal",
    },
  },
  android: {
    bold: {
      fontFamily: "AzoSans-Bold",
    },
    regular: {
      fontFamily: "AzoSans-Regular",
      fontWeight: "normal",
    },
    medium: {
      fontFamily: "AzoSans-Medium",
      fontWeight: "normal",
    },
    light: {
      fontFamily: "AzoSans-Light",
      fontWeight: "normal",
    },
    thin: {
      fontFamily: "AzoSans-Thin",
      fontWeight: "normal",
    },
  },
};
const theme = {
  ...DefaultTheme,
  dark: true,
  roundness: 2,
  fonts: configureFonts(fontConfig),
  colors: {
    ...DefaultTheme.colors,
    primary: "#2a3d6f",
    accent: "#75842f",
  },
};

export default function App() {
  const [loaded, error] = useFonts({
    "AzoSans-Regular": require("./assets/fonts/AzoSansRegular.ttf"),
    "AzoSans-Bold": require("./assets/fonts/AzoSans-Bold.ttf"),
    "AzoSans-Thin": require("./assets/fonts/AzoSans-Thin.ttf"),
    "AzoSans-Medium": require("./assets/fonts/AzoSans-Medium.ttf"),
  });
  const navigationRef = useRef();
  const routeNameRef = useRef();
  const routeTitleRef = useRef();

  if (!loaded) {
    return <AppLoading />;
  } else {
    return (
      <PaperProvider theme={theme}>
        <NavigationContainer
          ref={navigationRef}
          onReady={() => {
            routeNameRef.current = navigationRef.current.getCurrentRoute().name;
            routeTitleRef.current =
              navigationRef.current.getCurrentOptions().title;
          }}
          onStateChange={async () => {
            //for route name:
            const previousRouteName = routeNameRef.current;
            const currentRouteName =
              navigationRef.current.getCurrentRoute().name;

            //for route title:
            const previousTitle = routeTitleRef.current;
            const currentTitle =
              navigationRef.current.getCurrentOptions().title;

            if (
              previousRouteName !== currentRouteName ||
              previousTitle !== currentTitle
            ) {
              // The line below uses the expo-firebase-analytics tracker
              // https://docs.expo.io/versions/latest/sdk/firebase-analytics/
              // Change this line to use another Mobile analytics SDK
              console.log("current route name", currentRouteName);
              await Analytics.setCurrentScreen(currentRouteName);
              await Analytics.logEvent("screen_view", {
                screen_name: currentRouteName,
                screen_class: "MainActivity",
              });
              await Analytics.logEvent("user_engagement", {
                screen_name: currentRouteName,
                screen_class: "MainActivity",
              });
            }

            // Save the current route name for later comparison
            routeNameRef.current = currentRouteName;
            routeTitleRef.current = currentTitle;
          }}
        >
          <Index />
        </NavigationContainer>
      </PaperProvider>
    );
  }
}
