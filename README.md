# Power Water Mobile App

##### Version 3.0

### Technology stack

* Node v14.15.0
* yarn v1.22.10
* React Native
* Expo v4.4.4 (SDK 41)
* Twitter API
* Google maps javascript API

## 1. Installation

### Step 1: Install the Prerequisites

- Install Node.js
- Install and upgrade npm

```
    npm install npm 
```

- Install Yarn.js

```
    npm install -g yarn
```

- Install `expo-cli`

```
    npm install expo-cli
```

### Step 2: Clone this repo

```
    git clone git@bitbucket.org:pwc-app/mobile-app-development.git
```

### Step 3: Install the Dependencies

Go inside the project folder and run:

```
    yarn add
```

### step 4: Run the code

Run expo command either using:

``` 
    yarn start 
```

or

```
    expo start
```

### Step 5: Download the Expo and Run the program

Open either play store or App store and install **Expo Go** app

### Step 6: Scan the QR Code and run the App

****

## 2. Build

make publishable builds using expo SDK

### 2.1: To make android Build

```
    expo build:android
```

- provide the keystore credentials and select apk bundle. The build is done in expo servers and can be downloaded later
  from expo.io.

- Once the build is completed, download the apk file and upload to the google play console.

### 2.1: To make ios Build

```
    expo build:ios
```

- provide the app store credentials and select publishable package option.
- Once the build is completed, download the ipa file and upload to app store using the TestFlight applications available
  in macOS.

## 3. Publish

****

### make sure all of the latest change are in the master branch

### 3.0 Change the build/version codes for android and iOS in app.json
- make sure you're selecting the same number pattern for both ios and android
- For ios, change the build number
![img_1.png](assets/img/bnumber_ios.png)

- for android, change the version code:
![img_1.png](assets/img/vcode_android.png)

### 3.1 checkout to production branch
```` git checkout production ````

````git merge master````

### 3.2 create and expo build
for iOS
```expo build:ios --release-channel pwc-production ```

for android ```expo build:android --release-channel pwc-production```

### 3.3 go to 
[http://expo.io](Expo Login) Login

**Username**:yuba@cloudst.com.au

**Password**: bitwardern

Go to builds and download the latest builds for ios and android


### 3.4 Upload to Android play store
go to [https://play.google.com/console/u/3/developers]()

use Power and Water corporation developers account


### 3.5 Upload to App store






## 4. App structure
Major modules and files:

![img.png](img.png)

**App.js**: Entry point for the app

**index.js**: contains all the app routes and screen definitions 

**src**: main folder that contains all the application related code


**



****

## 5. Form submissions

For all type of forms, the payload structure is same. The forms are differentiated using the FormID.

Currently, Form IDs used in the App are:

Form Type | Form | FormID
--- | --- | --- 
Meter Readings | Mechanical | 8
Meter Readings | Electronic | 8
Meter Readings | Solar | 8
Meter Readings | Water | 7
| | |
Report Fault | Power | 15
Report Fault | Water | 4
Report Fault | Sewerage | 20
Report Fault | Streetlight | 3

**Example Request payload**

```json
{
  FormID: formId,
  Fields: [
    {
      FieldName: "date",
      FieldValue: date
      ?
      .date
      |
      ""
    },
    {
      FieldName: "Meter Type",
      FieldValue: meterType
    },
    {
      FieldName: "image",
      FieldValue: photo
      ?
      .localUri
      |
      ""
    },
    ...
    }
```

example request:

```javascript
    const submission = await axios.post(PW_BACKEND_URI + API_KEY, payload, {
  headers: {
    Accept: "application/json",
  },
});
```


