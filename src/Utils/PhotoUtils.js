//ACCESS CAMERA
import * as ImagePicker from "expo-image-picker";
import { Alert } from "react-native";

export const openCameraAsync = async () => {
  let permissionResult = await ImagePicker.requestCameraPermissionsAsync();

  if (permissionResult.granted === false) {
    Alert.alert("Permission to access camera roll is required!");
    return;
  }
  try {
    let cameraResult = await ImagePicker.launchCameraAsync({
      base64: true,
      aspect: [4, 3],
      allowsMultipleSelection: true,
      quality: 0.2,
    });
    if (cameraResult.cancelled === true) {
      return;
    }
    setModalVisible(false);
    setSelectedImage({
      localUri: cameraResult ? cameraResult.base64 : null,
    });
  } catch (e) {
    console.log(e);
  }
};

//IMAGE PICKER METHODS
export const openImagePickerAsync = async () => {
  let permissionResult =
    await ImagePicker.requestMediaLibraryPermissionsAsync();

  if (permissionResult.granted === false) {
    Alert.alert("Permission to access camera roll is required!");
    return;
  }
  try {
    let libraryResult = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      base64: true,
      aspect: [4, 3],
      allowsMultipleSelection: true,
      quality: 0.5,
    });
    if (libraryResult.cancelled === true) {
      return;
    }
    setModalVisible(false);
    setSelectedImage({
      localUri: libraryResult ? libraryResult.base64 : null,
    });
  } catch (e) {
    console.log(e);
  }
};
