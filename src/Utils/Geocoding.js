import Geocoder from "react-native-geocoding";

const API_KEY = "AIzaSyA0LcBOkG-cOB-LFi9eES6tYPk7voHjIJg";
Geocoder.init(API_KEY);

export async function convertPointToAddress(latitude, longitude) {
  try {
    const addressValue = await Geocoder.from([latitude, longitude]);

    return addressValue.results[0].formatted_address;
  } catch (e) {
    console.log("Error in geocoding", e);
    // throw new Error(e);
    return "N/A";
  }
}
