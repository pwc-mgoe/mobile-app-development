import qs from "qs";
import axios from "axios";

//PAYMENT CONSTANTS
const PW_FINGERPRINT_REQUEST_API_KEY = "d43e4fc2-604e-4f56-a567-75c7d71dde72";
const PW_FINGERPRINT_PAYMENT_REQUEST_URI = "http://webservices.powerwater.com.au/b_auth_sec.php";
const NAB_DIRECTPOST_URI = "https://transact.nab.com.au/live/directpostv2/authorise";

// //TEST APIs
// const PW_FINGERPRINT_PAYMENT_REQUEST_URI = "http://webservices.powerwater.com.au/b_auth_sec_test.php";
// const NAB_DIRECTPOST_URI = "https://demo.transact.nab.com.au/live/directpostv2/authorise";

const FINGERPRINT_TYPE = "payment";
const nabHeader = {
    "Content-Type": "application/x-www-form-urlencoded",
};

//REQUEST FINGERPRINT
async function MakeFingerprintRequest(
    CReferenceNumber,
    FingerPrintAmount,
    responseCode
) {
    let requestURI;
    if (responseCode) {
        requestURI =
            PW_FINGERPRINT_PAYMENT_REQUEST_URI +
            "?TYPE=" +
            FINGERPRINT_TYPE +
            "&REFERENCE=" +
            CReferenceNumber +
            "&AMOUNT=" +
            FingerPrintAmount +
            "&RESPONSECODE=" +
            responseCode +
            "&API=" +
            PW_FINGERPRINT_REQUEST_API_KEY;
    } else {
        requestURI =
            PW_FINGERPRINT_PAYMENT_REQUEST_URI +
            "?TYPE=" +
            FINGERPRINT_TYPE +
            "&REFERENCE=" +
            CReferenceNumber +
            "&AMOUNT=" +
            FingerPrintAmount +
            "&API=" +
            PW_FINGERPRINT_REQUEST_API_KEY;
    }

    // console.log(requestURI);
    try {
        const { data } = await axios.get(requestURI, {
            headers: {
                "Content-Type": "application/json",
            },
        });
        return data;
    } catch (e) {
        console.log(e);
    }
}

//sanitise amount value
function sanitiseAmount(amount) {
    if (amount.includes(".")) {
        if (amount.split(".")[1].length < 2) {
            return amount + "0"
        } else if (amount.split(".")[1].length > 2) {
            return amount.split(".")[0] + "." + amount.split(".")[1].substring(0, 2)
        } else {
            return amount;
        }

    } else {
        return amount + ".00";
    }
}

///MAIN FUNCTION TO PROCESS THE PAYMENT
export async function MakePayment(paymentDetails) {
    // console.log(paymentDetails);
    const { crnNumber, cardNumber, expiration, cvv, amount } = paymentDetails;

    const [expiryMonth, expiryYear] = expiration.split("/");

    const newAmount = sanitiseAmount(amount);
    // console.log(newAmount, typeof newAmount, typeof amount);
    const fpResponse = await MakeFingerprintRequest(crnNumber, newAmount);

    // console.log(fpResponse);
    const payload = {
        EPS_MERCHANT: fpResponse.Merchant,
        EPS_TXNTYPE: 0, // refer to NAB direct post guide for more options
        EPS_REFERENCEID: crnNumber,
        EPS_AMOUNT: newAmount,
        EPS_TIMESTAMP: fpResponse.TimeStamp,
        EPS_FINGERPRINT: fpResponse.FingerPrint,
        EPS_RESULTURL: fpResponse.Redirect,
        EPS_CARDNUMBER: cardNumber,
        EPS_EXPIRYMONTH: expiryMonth,
        EPS_EXPIRYYEAR: expiryYear,
        EPS_CCV: cvv,
    };

    const nabConfig = {
        method: "POST",
        headers: {
            ...nabHeader,
        },
        data: qs.stringify(payload),
        url: NAB_DIRECTPOST_URI,
    };

    try {
        const { data } = await axios(nabConfig);

        // console.log(data);

        // Payment Response Code Groups
        const errorCategory = ["00", "08", "11"];

        const errorCategoryOne = ["01" , "02" , "06" , "07" , "12" , "13" , "14" , "15" , "19" , "20" , "21" , "22" , "25" , "26" , "27" , "28" , "29" , "33" , "34" , "35" , "36" , "37" , "38" , "39" , "40" , "51" , "52" , "53" , "54" , "55" , "56" , "57" , "59" , "61" , "62" , "64" , "65" , "68" , "91" , "92" , "93" , "94" , "95" , "100" , "101" , "102" , "103" , "106" , "109" , "114" , "132" , "162"]

        const errorCategoryTwo = ["03" , "04" , "05" , "09" , "10" , "17" , "18" , "23" , "24" , "30" , "31" , "32" , "41" , "42" , "44" , "67" , "75" , "86" , "87" , "88" , "89" , "90" , "96" , "97" , "98" , "99" , "110" , "111" , "112" , "113" , "115" , "116" , "117" , "118" , "119" , "133" , "134" , "135" , "136" , "137" , "138" , "139" , "140" , "141" , "142" , "143" , "144" , "146" , "157" , "158" , "159" , "163" , "164" , "165" , "166" , "167" , "168" , "169" , "179" , "505" , "510" , "511" , "512" , "513" , "514" , "515" , "516" , "517" , "524" , "545" , "550" , "575" , "577" , "580" , "594" , "595"]

        const errorCategoryThree = ["43" , "58" , "60" , "63" , "66" , "104" , "123" , "124" , "125" , "126" , "131" , "145" , "147" , "148" , "149" , "151" , "152" , "153" , "160" , "175" , "176" , "177" , "178" , "190" , "195" , "199" , "504"]

        if (errorCategory.includes(data.rescode)) {
            const fpPaymentResponseFingerprint = await MakeFingerprintRequest(
                crnNumber,
                amount,
                data.summarycode
            );
            // console.log(fpPaymentResponseFingerprint);
            return data;
        } else if (errorCategoryOne.includes(data.rescode)) {
            throw Error(
                "Your payment has been declined. Please contact your bank for more information. Error code: 01-" +
                data.rescode
            );
        } else if (errorCategoryTwo.includes(data.rescode)) {
            throw Error(
                "Your payment has been declined. Please contact your bank for more information. Error code: 03-" +
                data.rescode
            );
        } else if (errorCategoryThree.includes(data.rescode)) {
            throw Error(
                "Your payment has been declined. Please contact your bank for more information.  Error code: 43-" +
                data.rescode
            );
        } else {
            throw Error(
                "Unable to submit payment, please contact our Contact Centre on 1800 245 092."
            );
        }
    } catch (e) {
        throw e;
    }
}

export default { MakePayment };