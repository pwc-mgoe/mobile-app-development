import axios from "axios";
import moment from "moment";

export const PW_BACKEND_URI =
  "http://webservices.powerwater.com.au/WebservicePTP/index.php/SubmitForm?api_key=";
export const API_KEY = "v68vUfMS*VMZA6B8";

export async function SubmitReportFaultForm(formId, fault, formValues, photo) {
  console.log(formId, fault, formValues);
  const payload = {
    FormID: formId,
    Fields: [
      {
        FieldName: "Date",
        FieldValue: formValues.date
          ? moment(formValues.date).format("dddd, Do MMMM YYYY, LTS")
          : "",
      },
      {
        FieldName: "Fault",
        FieldValue: fault ? fault : "",
      },
      {
        FieldName: "Address",
        FieldValue: formValues.address ? formValues.address : "",
      },
      {
        FieldName: "Phone",
        FieldValue: formValues.phone ? formValues.phone : "",
      },
      {
        FieldName: "Email",
        FieldValue: formValues.email ? formValues.email : "",
      },
      {
        FieldName: "Additional comments",
        FieldValue: formValues.comment ? formValues.comment : "",
      },
      {
        FieldName: "Access related issues",
        FieldValue: formValues.accessIssue ? formValues.accessIssue : "",
      },
      {
        FieldName: "Acknowledgement",
        FieldValue: "Yes",
      },
      formValues?.streetlight && {
        FieldName: "Serial number",
        FieldValue: formValues.serial ? formValues.serial : "",
      },
    ],
  };
  photo.forEach((image) =>
    payload.Fields.push({
      FieldName: "image",
      FieldValue: image,
    })
  );

  try {
    const submission = await axios.post(PW_BACKEND_URI + API_KEY, payload, {
      headers: {
        Accept: "application/json",
      },
    });
    return submission.status;
  } catch (e) {
    console.log(e);
  }
}
