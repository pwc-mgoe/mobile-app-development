import axios from "axios";
import { PW_BACKEND_URI, API_KEY } from "./Config";
import moment from "moment";

//format the dates
const genDate = (formValues) => {
  let xDate = new Date(formValues.year, formValues.month - 1, formValues.day);

  const tFormatted =
    moment(xDate).format("dddd, Do MMMM YYYY") + ", " + moment().format("LTS");
  return tFormatted;
};

export async function SubmitForm(
  formId,
  meter,
  meterType,
  formValues,
  date,
  photo
) {
  let payload = {};

  if (meterType === "three-phase") {
    payload = {
      FormID: formId,
      Fields: [
        {
          FieldName: "Date",
          FieldValue: formValues ? genDate(formValues) : " ",
        },
        {
          FieldName: "Meter",
          FieldValue: meter ? meter : "",
        },
        {
          FieldName: "Meter type",
          FieldValue: meterType,
        },
        {
          FieldName: "Meter number 1",
          FieldValue: formValues.meternumber ? formValues.meternumber : "",
        },
        {
          FieldName: "Meter reading",
          FieldValue: formValues.meterreading ? formValues.meterreading : "",
        },
        {
          FieldName: "Meter number 2",
          FieldValue: formValues.meternumber2 ? formValues.meternumber2 : "",
        },
        {
          FieldName: "Meter reading",
          FieldValue: formValues.meterreading2 ? formValues.meterreading2 : "",
        },
        {
          FieldName: "Meter number 3",
          FieldValue: formValues.meternumber3 ? formValues.meternumber3 : "",
        },
        {
          FieldName: "Meter reading",
          FieldValue: formValues.meterreading3 ? formValues.meterreading3 : "",
        },
        {
          FieldName: "Phone",
          FieldValue: formValues.phone ? formValues.phone : "",
        },
        {
          FieldName: "Email",
          FieldValue: formValues.email ? formValues.email : "",
        },
        {
          FieldName: "Acknowledgement",
          FieldValue: "Yes",
        },
      ],
    };
    photo.forEach((image) =>
      payload.Fields.push({
        FieldName: "image",
        FieldValue: image,
      })
    );
  } else {
    payload = {
      FormID: formId,
      Fields: [
        {
          FieldName: "Date",
          FieldValue: formValues ? genDate(formValues) : " ",
        },
        {
          FieldName: "Meter",
          FieldValue: meter ? meter : "",
        },
        meter !== "Water Meter" && {
          FieldName: "Meter type",
          FieldValue:
            meter === "Electronic meter" ? "Time of use" : "single-phase",
        },
        {
          FieldName: "Meter number",
          FieldValue: formValues.meternumber ? formValues.meternumber : "",
        },
        {
          FieldName:
            meter === "Electronic meter" ? "04 reading" : "Meter reading",
          FieldValue: formValues.meterreading ? formValues.meterreading : "",
        },
        meter === "Electronic meter" && {
          FieldName: "10 reading",
          FieldValue: formValues.meterreading10
            ? formValues.meterreading10
            : "",
        },
        {
          FieldName: "Phone",
          FieldValue: formValues.phone ? formValues.phone : "",
        },
        {
          FieldName: "Email",
          FieldValue: formValues.email ? formValues.email : "",
        },
        {
          FieldName: "Acknowledgement",
          FieldValue: "Yes",
        },
      ],
    };
    photo.forEach((image) =>
      payload.Fields.push({
        FieldName: "image",
        FieldValue: image,
      })
    );
  }

  try {
    const submission = await axios.post(PW_BACKEND_URI + API_KEY, payload, {
      headers: {
        Accept: "application/json",
      },
    });
    return submission.status;
  } catch (e) {
    console.log(e);
  }
}

export async function SubmitSolarForm(
  formId,
  meter,
  meterType,
  formValues,
  date,
  photo
) {
  let payload = {};
  if (meterType === "three-phase") {
    payload = {
      FormID: formId,
      Fields: [
        {
          FieldName: "Date",
          FieldValue: formValues ? genDate(formValues) : " ",
        },
        {
          FieldName: "Meter",
          FieldValue: meter ? meter : "",
        },
        {
          FieldName: "Meter type",
          FieldValue: meterType,
        },
        {
          FieldName: "Meter number 1",
          FieldValue: formValues.meternumber ? formValues.meternumber : "",
        },
        {
          FieldName: "003 reading",
          FieldValue: formValues.meterreading003
            ? formValues.meterreading003
            : "",
        },
        {
          FieldName: "023 reading",
          FieldValue: formValues.meterreading023
            ? formValues.meterreading023
            : "",
        },
        {
          FieldName: "Meter number 2",
          FieldValue: formValues.meternumber2 ? formValues.meternumber2 : "",
        },
        {
          FieldName: "003 reading",
          FieldValue: formValues.meterreading0032
            ? formValues.meterreading0032
            : "",
        },
        {
          FieldName: "023 reading",
          FieldValue: formValues.meterreading0232
            ? formValues.meterreading0232
            : "",
        },
        {
          FieldName: "Meter number 3",
          FieldValue: formValues.meternumber3 ? formValues.meternumber3 : "",
        },
        {
          FieldName: "003 reading",
          FieldValue: formValues.meterreading0033
            ? formValues.meterreading0033
            : "",
        },
        {
          FieldName: "023 reading",
          FieldValue: formValues.meterreading0233
            ? formValues.meterreading0233
            : "",
        },
        {
          FieldName: "Phone",
          FieldValue: formValues.phone ? formValues.phone : "",
        },
        {
          FieldName: "email",
          FieldValue: formValues.email ? formValues.email : "",
        },
        {
          FieldName: "Acknowledgement",
          FieldValue: "Yes",
        },
      ],
    };
    photo.forEach((image) =>
      payload.Fields.push({
        FieldName: "image",
        FieldValue: image,
      })
    );
  } else if (meterType === "combination") {
    payload = {
      FormID: formId,
      Fields: [
        {
          FieldName: "Date",
          FieldValue: formValues ? genDate(formValues) : " ",
        },
        {
          FieldName: "Meter",
          FieldValue: meter ? meter : "",
        },
        {
          FieldName: "Meter type",
          FieldValue: meterType,
        },
        {
          FieldName: "Meter number",
          FieldValue: formValues.meternumber ? formValues.meternumber : "",
        },
        {
          FieldName: "04 reading ",

          FieldValue: formValues.meterreading04
            ? formValues.meterreading04
            : "",
        },
        {
          FieldName: "010 reading ",
          FieldValue: formValues.meterreading010
            ? formValues.meterreading010
            : "",
        },
        {
          FieldName: "023 reading ",
          FieldValue: formValues.meterreading023
            ? formValues.meterreading023
            : "",
        },
        {
          FieldName: "Phone",
          FieldValue: formValues.phone ? formValues.phone : "",
        },
        {
          FieldName: "email",
          FieldValue: formValues.email ? formValues.email : "",
        },
        {
          FieldName: "Acknowledgement",
          FieldValue: "Yes",
        },
      ],
    };
    photo.forEach((image) =>
      payload.Fields.push({
        FieldName: "image",
        FieldValue: image,
      })
    );
  } else {
    payload = {
      FormID: formId,
      Fields: [
        {
          FieldName: "Date",
          FieldValue: formValues ? genDate(formValues) : " ",
        },
        {
          FieldName: "Meter",
          FieldValue: meter ? meter : "",
        },
        {
          FieldName: "Meter type",
          FieldValue: meterType,
        },
        {
          FieldName: "Meter number",
          FieldValue: formValues.meternumber ? formValues.meternumber : "",
        },
        {
          FieldName: "003 reading",
          FieldValue: formValues.meterreading003
            ? formValues.meterreading003
            : "",
        },
        {
          FieldName: "023 reading",
          FieldValue: formValues.meterreading023
            ? formValues.meterreading023
            : "",
        },
        {
          FieldName: "Phone",
          FieldValue: formValues.phone ? formValues.phone : "",
        },
        {
          FieldName: "email",
          FieldValue: formValues.email ? formValues.email : "",
        },
        {
          FieldName: "Acknowledgement",
          FieldValue: "Yes",
        },
      ],
    };
    photo.forEach((image) =>
      payload.Fields.push({
        FieldName: "image",
        FieldValue: image,
      })
    );
  }

  try {
    const submission = await axios.post(PW_BACKEND_URI + API_KEY, payload, {
      headers: {
        Accept: "application/json",
      },
    });
    return submission.status;
  } catch (e) {
    console.log(e);
  }
}
