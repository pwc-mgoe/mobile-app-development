import React, { useState, useEffect } from "react";
import { StyleSheet, Dimensions, Alert, Platform } from "react-native";
import { Text } from "react-native-paper";
import MapView, { Marker } from "react-native-maps";
import * as Location from "expo-location";
import { convertPointToAddress } from "../Utils/Geocoding";
import { ActivityIndicator } from "react-native-paper";
import * as Style from "../Components/styles/Style";
// DELTA Values for map
const latitudeDelta = 0.25;
const longitudeDelta = 0.25;

export default function MapNew(props) {
  const [location, setLocation] = useState(null);

  // async function getLocationAsync() {
  //   // permissions returns only for location permissions on iOS and under certain conditions, see Permissions.LOCATION
  //   const { status, permissions } = await Permissions.askAsync(Permissions.LOCATION_FOREGROUND);
  //   if (status === 'granted') {
  //     console.warn( await Location.getCurrentPositionAsync({ enableHighAccuracy: true }));
  //   } else {
  //     throw new Error('Location permission not granted');
  //   }
  // }

  ///Change address value when the map is moved
  async function changeAddress(e) {
    const { coordinate } = e.nativeEvent;
    try {
      const address =
        (await convertPointToAddress(
          coordinate.latitude,
          coordinate.longitude
        )) || "NA";
      props.handleAddressChange(address);
    } catch (e) {
      const address = "";
      props.handleAddressChange(address);
    }
  }

  //return a default value for maps if location is not granted/ obtained.
  function handleLocationNotFound() {
    // Alert.alert('Error fetching the location.', 'Please check your location permission');
    console.log("Error fetching location");
    const locationOps = {
      latitudeDelta: latitudeDelta,
      longitudeDelta: longitudeDelta,
      latitude: -12.388049,
      longitude: 130.851038,
    };
    setLocation(locationOps);
  }

  //handle the location change.
  async function handleLocationChange() {
    // let locationCords = await Location.getLastKnownPositionAsync();
    // if (!locationCords) {
    //   locationCords = await Location.getCurrentPositionAsync({enableHighAccuracy: true}
    //   );
    // }
    let locationCords = await Location.getCurrentPositionAsync({
      accuracy: Location.Accuracy.High,
    });

    const locationOps = {
      latitudeDelta: latitudeDelta,
      longitudeDelta: longitudeDelta,
      latitude: locationCords.coords.latitude,
      longitude: locationCords.coords.longitude,
    };

    setLocation(locationOps);

    const address = await convertPointToAddress(
      locationOps.latitude,
      locationOps.longitude
    );

    props.handleAddressChange(address);
  }

  useEffect(() => {
    // getLocationAsync();
    (async () => {
      try {
        //check if the service is enabled
        let status = await Location.hasServicesEnabledAsync();
        if (!status) {
          handleLocationNotFound();
        } else {
          //check if the location permission is already granted

          if (Platform.OS === "android") {
            let permissionStatus =
              await Location.getForegroundPermissionsAsync();
            status = permissionStatus.status;
          } else {
            let { status } = await Location.getForegroundPermissionsAsync();
          }
          if (status === "granted") {
            await handleLocationChange();
          } else {
            // ask for permission
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status === "granted") {
              await handleLocationChange();
            } else {
              handleLocationNotFound();
            }
          }
        }
      } catch (err) {
        // handleLocationNotFound();
        Alert.alert(err.message);
      }
    })();
  }, [Location]);

  return location === null ? (
    <>
      <ActivityIndicator />
    </>
  ) : (
    <>
      <MapView
        initialRegion={location}
        style={props.style}
        addressForCoordinate={(e) => address(e)}
        minZoomLevel={15}
      >
        <Marker
          draggable
          key={1}
          coordinate={{
            latitude: location ? location.latitude : 0,
            longitude: location ? location.longitude : 0,
          }}
          onDragEnd={async (e) => {
            changeAddress(e);
            props.handleCoordinatesChange(e);
          }}
        />
      </MapView>
      <Text style={{ textAlign: "center", color: Style.COLOR_PRIMARY }}>
        Hold and drag marker to select the location
      </Text>
    </>
  );
}
