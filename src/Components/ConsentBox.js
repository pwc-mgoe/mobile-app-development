import React, { useEffect, useState } from "react";
import { Checkbox, Text } from "react-native-paper";
import * as Style from "./styles/Style";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import PropTypes from "prop-types";

function ConsentBox({ onPress }) {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    onPress(checked);
  }, [checked]);

  const __handleBoxPress = () => {
    setChecked(!checked);
    onPress(checked);
  };
  return (
    <TouchableOpacity style={{ margin: Style.GAP }} onPress={__handleBoxPress}>
      <Text style={styles.consentTitle}>I/We understand that:</Text>
      <View style={styles.consentContainer}>
        <View style={styles.checkBoxWrapper}>
          <Checkbox.Item
            color={Style.COLOR_PRIMARY}
            status={checked ? "checked" : "unchecked"}
            onPress={__handleBoxPress}
            style={styles.checkBoxStyle}
            mode="android"
            required={true}
          />
        </View>
        <Text style={styles.consentDescription}>
          Power and Water Corporation may store personal information in paper
          and electronic formats. Power and Water takes all reasonable steps to
          ensure the security of all customer data. Personal information is
          maintained in a secure environment accessed only by authorised
          personnel. However, any data sent over the internet or store on
          internet servers cannot be guaranteed to be fully secure. These
          activities are undertaken at your risk.
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  consentContainer: {
    // flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "stretch",
    justifyContent: "flex-start",
  },
  consentTitle: {
    fontSize: Style.FONT_SIZE_CARD,
    marginBottom: Style.GAP,
  },
  checkBoxWrapper: {
    width: 40,
    alignSelf: "flex-start",
  },
  checkBoxStyle: {
    borderColor: Style.COLOR_PRIMARY,
    alignSelf: "flex-end",
    height: 30,
  },
  consentDescription: {
    flex: 5,
    alignSelf: "center",
    justifyContent: "flex-start",
    textAlign: "justify",
    flexWrap: "wrap",
    fontSize: Style.FONT_SIZE_BODY,
  },
});

ConsentBox.propTypes = {
  onPress: PropTypes.func,
};

export default ConsentBox;
