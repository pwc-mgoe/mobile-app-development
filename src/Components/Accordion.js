import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native";
import { Headline, List } from "react-native-paper";
import * as Style from "./styles/Style";

export default function Accordion(props) {
  let { list, headline } = props;
  const [expanded, setExpanded] = useState(false);
  const [accordionTitle, setAccordionTitle] = useState("--Please select--");
  const [disable, setDisable] = useState(false);

  return (
    <>
      <Headline style={styles.headline}>{headline}</Headline>
      <List.Section>
        <List.Accordion
          disabled={disable}
          expanded={expanded}
          title={accordionTitle}
          style={disable ? styles.disabled : styles.accordion}
          titleNumberOfLines={3}
          onPress={() => !disable && setExpanded(!expanded)}
        >
          {(list.children || list || []).map((item) => (
            <List.Item
              disabled={disable}
              key={item.name}
              title={item.value}
              titleNumberOfLines={3}
              style={styles.listItem}
              onPress={() => {
                setExpanded(false);
                props.onItemPressed(item);
                setAccordionTitle(item.value);
                setDisable(true);
              }}
            />
          ))}
        </List.Accordion>
      </List.Section>
    </>
  );
}

const styles = StyleSheet.create({
  accordion: {
    borderWidth: 2,
    borderColor: Style.COLOR_PRIMARY,
    backgroundColor: "#FFFFFF",
  },
  listItem: {
    borderWidth: 1,
    borderColor: Style.COLOR_SECONDARY,
  },
  headline: {
    fontSize: Style.FONT_SIZE_CARD,
    marginTop: Style.GAP,
    fontFamily: Style.FONT_BOLD,
  },
  disabled: {
    borderWidth: 2,
    borderColor: Style.COLOR_PRIMARY,
    backgroundColor: "#eeeeee",
  },
});
