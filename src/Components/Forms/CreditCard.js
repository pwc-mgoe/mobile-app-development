import * as React from "react";
import { FormProvider, useForm } from "react-hook-form";
import {
  Alert,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  View
} from "react-native";
import LottieView from "lottie-react-native";
import CreditCardForm, { Button, FormModel } from "rn-credit-card";
import Header from "../Components/Header";
import BlockQuote from "../Components/BlockQuote";
import * as Style from "../Components/styles/Style";
import { Text, TextInput } from "react-native-paper";


export default function CreditCard() {
  const formMethods = useForm({
    // to trigger the validation on the blur event
    mode: "onBlur",
    defaultValues: {
      holderName: "",
      cardNumber: "",
      expiration: "",
      cvv: ""
    }
  });
  const { handleSubmit, formState } = formMethods;

  function onSubmit(model: FormModel) {
    Alert.alert("Success: " + JSON.stringify(model, null, 2));
  }

  return (
    <FormProvider {...formMethods}>
      <Header title="Make Payment" />
      import * as React from "react";
import { FormProvider, useForm } from "react-hook-form";
import {
  Alert,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  View
} from "react-native";
import LottieView from "lottie-react-native";
import CreditCardForm, { Button, FormModel } from "rn-credit-card";
import Header from "../Components/Header";
import BlockQuote from "../Components/BlockQuote";
import * as Style from "../Components/styles/Style";
import { Text, TextInput } from "react-native-paper";


export default function CreditCard() {
  const formMethods = useForm({
    // to trigger the validation on the blur event
    mode: "onBlur",
    defaultValues: {
      holderName: "",
      cardNumber: "",
      expiration: "",
      cvv: ""
    }
  });
  const { handleSubmit, formState } = formMethods;

  function onSubmit(model: FormModel) {
    Alert.alert("Success: " + JSON.stringify(model, null, 2));
  }

  return (
    <FormProvider {...formMethods}>
      <Header title="Make Payment" />
      <Formik
          validationSchema={formValidationSchema}
          initialValues={{
            date: "",
            meternumber: "",
            meterreading: "",
            phone: "",
            email: ""
          }}
          onSubmit={values =>
            handlFormSubmit(meter, meterType, values, date, selectedImage)
          }
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            errors,
            isValid
          }) => (
            <View>
              {show && (
                <RNDateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode="date"
                  display="default"
                  onChange={onChange}
                  style={{
                    flex: 1,
                    alignContent: "center",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                />
              )}
              {modalVisible && (
                <View>
                  <Portal>
                    <Modal
                      visible={modalVisible}
                      onDismiss={hideModal}
                      contentContainerStyle={containerStyle}
                    >
                      <Button
                        mode="contained"
                        style={styles.footerContainer}
                        icon="camera"
                        onPress={openCameraAsync}
                      >
                        From Camera
                      </Button>
                      <Button
                        mode="contained"
                        style={styles.footerContainer}
                        icon="upload"
                        onPress={openImagePickerAsync}
                      >
                        Upload from Gallery
                      </Button>
                      <Button
                        mode="contained"
                        style={[
                          styles.footerContainer,
                          { backgroundColor: "red" }
                        ]}
                        icon="close"
                        onPress={hideModal}
                      >
                        Cancel
                      </Button>
                    </Modal>
                  </Portal>
                </View>
              )}
              <View>
                {!show && (
                  <Button onPress={showMode}>Choose date of Reading</Button>
                )}
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: Style.FONT_SIZE_HEADING,
                    padding: Style.GAP
                  }}
                >
                  {date.toDateString()}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  margin: Style.GAP,
                  backgroundColor: "#EAEAEA"
                }}
              >
                <Button mode={singlePhaseButtonMode} onPress={onButtonToggle}>
                  single phase
                </Button>
                <Button mode={threePhaseButtonMode} onPress={onButtonToggle}>
                  three phase
                </Button>
              </View>
              <Text style={styles.textLabel}>Your meter number</Text>
              <TextInput
                name="meternumber"
                label="your meter number"
                mode="outlined"
                onChangeText={handleChange("meternumber")}
                onBlur={handleBlur("meternumber")}
                value={values.meternumber}
                style={styles.textInput}
                keyboardType="numeric"
              />
              {errors.meternumber && (
                <Text style={styles.error}>{errors.meternumber}</Text>
              )}
              <Text style={styles.textLabel}>Meter reading</Text>
              <TextInput
                name="meterreading"
                label="Meter reading"
                mode="outlined"
                onChangeText={handleChange("meterreading")}
                onBlur={handleBlur("meterreading")}
                value={values.meterreading}
                style={styles.textInput}
                keyboardType="numeric"
              />
              {errors.meterreading && (
                <Text style={styles.error}>{errors.meterreading}</Text>
              )}
              <Text style={styles.textLabel}>Your phone number</Text>
              <TextInput
                name="phone"
                label="(ex: 0400111000)"
                mode="outlined"
                onChangeText={handleChange("phone")}
                onBlur={handleBlur("phone")}
                value={values.phone}
                style={styles.textInput}
                keyboardType="numeric"
              />
              {errors.phone && <Text style={styles.error}>{errors.phone}</Text>}
              <Text style={styles.textLabel}>Your email address</Text>
              <TextInput
                name="email"
                label="(ex: your@email.com)"
                mode="outlined"
                onChangeText={handleChange("email")}
                onBlur={handleBlur("email")}
                value={values.email}
                style={styles.textInput}
                keyboardType="email-address"
              />
              {errors.email && <Text style={styles.error}>{errors.email}</Text>}
              <View style={styles.footerContainer}>
                <Button mode="contained" icon="camera" onPress={showModal}>
                  Upload Photo
                </Button>
              </View>
              {selectedImage && (
                <View style={styles.container}>
                  <Image
                    source={{ uri: selectedImage.localUri }}
                    style={styles.thumbnail}
                  />
                </View>
              )}
              <TouchableOpacity
                onPress={handleSubmit}
                style={styles.footerContainer}
              >
                <Text
                  style={{
                    color: "#ffffff",
                    fontSize: 20
                  }}
                >
                  Submit
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </Formik>
    </FormProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  avoider: {
    flex: 1,
    padding: 36
  },
  button: {
    margin: 36,
    marginTop: 0
  }
});

    </FormProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  avoider: {
    flex: 1,
    padding: 36
  },
  button: {
    margin: 36,
    marginTop: 0
  }
});
