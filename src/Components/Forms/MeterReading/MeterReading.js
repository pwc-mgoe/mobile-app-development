import React, { useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  Alert,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import Header from "../../Header";
import { Formik } from "formik";
import {
  Text,
  TextInput,
  Button,
  Modal,
  Portal,
  Headline,
  ActivityIndicator,
} from "react-native-paper";
import * as Style from "../../styles/Style";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { SubmitForm } from "../../../Utils/FormUtils";
import { useNavigation } from "@react-navigation/native";
import moment from "moment";
import ConsentBox from "../../ConsentBox";
import {
  ElectronicValidation,
  PowerSinglePhaseValidation,
  PowerThreePhaseValidation,
  WaterValidation,
} from "../Validation/FormValidation";

import PhotoPicker from "../../PhotoPicker";

//MAIN COMPONENT
export default function MeterReading({ route }) {
  const { meter } = route.params;
  const navigation = useNavigation();
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [date, setDate] = useState(new Date());
  const [singlePhaseButtonMode, setSinglePhaseButtonMode] =
    useState("contained");
  const [threePhaseButtonMode, setThreePhaseButtonMode] = useState("text");
  const [meterType, setMeterType] = useState("single-phase");
  const [privacyCheckboxStatus, setPrivacyCheckboxStatus] = useState(null);
  const [loading, setLoading] = useState(false);

  const [selectedImageOne, setSelectedImageOne] = useState([]);
  const [selectedImageTwo, setSelectedImageTwo] = useState([]);
  const [selectedImageThree, setSelectedImageThree] = useState([]);
  const [formValidationSchema, setFormValidationSchema] = useState();

  // SINGLE AND THREEPHASE BUTTON TOGGLE
  //TODO: CREATE A SEPARATE REUSABLE COMPONENT FOR TOGGLE
  const onButtonToggle = (value) => {
    setSinglePhaseButtonMode(
      singlePhaseButtonMode === "contained" ? "text" : "contained"
    );
    setThreePhaseButtonMode(
      threePhaseButtonMode === "contained" ? "text" : "contained"
    );
    setMeterType(meterType === "single-phase" ? "three-phase" : "single-phase");
  };

  const containerStyle = {
    backgroundColor: "#ffffff",
    padding: 20,
    margin: Style.GAP,
  };

  const prepareImageSubmission = () => {
    let imageBundle = [];
    selectedImageOne?.map((image) => imageBundle.push(image.base64Uri));
    selectedImageTwo?.map((image) => imageBundle.push(image.base64Uri));
    selectedImageThree?.map((image) => imageBundle.push(image.base64Uri));

    return imageBundle;
  };

  useEffect(() => {
    prepareImageSubmission();
  }, [selectedImageOne, selectedImageTwo, selectedImageThree]);

  useEffect(() => {
    if (meter == "Standard mechanical meter" && meterType === "single-phase") {
      setFormValidationSchema(PowerSinglePhaseValidation);
    } else if (
      meter == "Standard mechanical meter" &&
      meterType === "three-phase"
    ) {
      setFormValidationSchema(PowerThreePhaseValidation);
    } else if (meter == "Electronic meter") {
      setFormValidationSchema(ElectronicValidation);
    } else if (meter == "Water Meter") {
      setFormValidationSchema(WaterValidation);
    }
  });

  //HANDLE FORM SUBMISSION
  const handleFormSubmit = async (meter, meterType, values, date) => {
    //Form Ids for different meter reading types
    let formId;
    if (meter === "Water Meter") {
      formId = 7;
    } else {
      formId = 8; //for power meters (mechanical, solar and electronic)
    }

    try {
      setLoading(true);
      const imageBundle = prepareImageSubmission();
      const result = await SubmitForm(
        formId,
        meter,
        meterType,
        values,
        selectedDate,
        imageBundle
      );
      setLoading(false);

      result === 200
        ? navigation.navigate("ThankYou", {
            msg: "Thank you for your submission.",
          })
        : Alert.alert("Error in submission.", " Please try again.");
    } catch (e) {
      Alert.alert(e);
    }
  };

  const handlePressPrivacyCheckboxStatus = (value) => {
    setPrivacyCheckboxStatus(value);
  };

  //RENDER MAIN SCREEN
  return (
    <>
      <Header title="Submit a meter read" />
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.container}
      >
        <ScrollView>
          <Headline style={{ alignSelf: "center", marginTop: Style.GAP }}>
            {meter ? meter : ""}
          </Headline>
          {loading && (
            <View>
              <Portal>
                <Modal
                  visible={loading}
                  style={{ opacity: 1 }}
                  contentContainerStyle={containerStyle}
                  onDismiss={() => {}}
                >
                  <ActivityIndicator animating={true} size="large" />
                  <Headline style={{ textAlign: "center", margin: Style.GAP }}>
                    Submission in progress. Please hold..
                  </Headline>
                </Modal>
              </Portal>
            </View>
          )}
          <Formik
            validationSchema={formValidationSchema}
            enableReinitialize={true}
            initialValues={{
              day: moment(selectedDate).format("DD"),
              month: moment(selectedDate).format("MM"),
              year: moment(selectedDate).format("yyyy"),
              meter: meter,
              metertype: meterType,
              meternumber: "",
              meterreading: "",
              meterreading10: "",
              meternumber2: "",
              meterreading2: "",
              meternumber3: "",
              meterreading3: "",
              phone: "",
              email: "",
              acknowledgement: "",
            }}
            onSubmit={(values) =>
              handleFormSubmit(meter, meterType, values, date)
            }
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
              touched,
              setFieldValue,
            }) => (
              <View>
                {!(meter === "Water Meter" || meter === "Electronic meter") && (
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      margin: Style.GAP,
                      backgroundColor: "#EAEAEA",
                    }}
                  >
                    <Button
                      mode={singlePhaseButtonMode}
                      onPress={onButtonToggle}
                      uppercase={false}
                    >
                      Single phase
                    </Button>
                    <Button
                      mode={threePhaseButtonMode}
                      onPress={onButtonToggle}
                      uppercase={false}
                    >
                      3 phase
                    </Button>
                  </View>
                )}
                <Text style={styles.textLabel}>Date of reading</Text>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <View style={{ flexDirection: "column", flex: 1 }}>
                    <TextInput
                      name="day"
                      label="dd"
                      mode="outlined"
                      onChangeText={handleChange("day")}
                      onBlur={handleBlur("day")}
                      value={values.day}
                      style={styles.dateInput}
                      maxLength={2}
                      accessibilityLabel="day"
                      keyboardType="numeric"
                    />
                    {errors.day && touched.day ? (
                      <Text style={styles.error}>{errors.day}</Text>
                    ) : null}
                  </View>
                  <View style={{ flexDirection: "column", flex: 1 }}>
                    <TextInput
                      name="month"
                      label="mm"
                      mode="outlined"
                      onChangeText={handleChange("month")}
                      onBlur={handleBlur("month")}
                      value={values.month}
                      keyboardType="numeric"
                      maxLength={2}
                      style={styles.dateInput}
                      accessibilityLabel="month"
                    />
                    {errors.month && touched.month ? (
                      <Text style={styles.error}>{errors.month}</Text>
                    ) : null}
                  </View>
                  <View style={{ flexDirection: "column", flex: 2 }}>
                    <TextInput
                      name="year"
                      label="yyyy"
                      mode="outlined"
                      onChangeText={handleChange("year")}
                      onBlur={handleBlur("year")}
                      value={values.year}
                      style={styles.yearInput}
                      keyboardType="numeric"
                      maxLength={4}
                      accessibilityLabel="year"
                    />
                    {errors.year && touched.year ? (
                      <Text style={styles.error}>{errors.year}</Text>
                    ) : null}
                  </View>
                </View>
                {meterType === "three-phase" ? (
                  <Text style={styles.textLabel}>Your meter number 1</Text>
                ) : (
                  <Text style={styles.textLabel}>Your meter number</Text>
                )}

                <TextInput
                  name="meternumber"
                  label=""
                  mode="outlined"
                  onChangeText={handleChange("meternumber")}
                  onBlur={handleBlur("meternumber")}
                  value={values.meternumber}
                  style={styles.textInput}
                  keyboardType={meter === "Water Meter" ? "default" : "numeric"}
                  maxLength={10}
                  accessibilityLabel="Meter number"
                />

                {errors.meternumber && touched.meternumber ? (
                  <Text style={styles.error}>{errors.meternumber}</Text>
                ) : null}

                {meter !== "Electronic meter" && (
                  <>
                    <Text style={styles.textLabel}>Meter reading</Text>

                    <TextInput
                      name="meterreading"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading")}
                      onBlur={handleBlur("meterreading")}
                      value={values.meterreading}
                      style={styles.textInput}
                      keyboardType={"numeric"}
                      maxLength={10}
                      accessibilityLabel="Meter reading"
                    />
                    {errors.meterreading && (
                      <Text style={styles.error}>{errors.meterreading}</Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageOne(val)}
                    />
                  </>
                )}

                {meter === "Electronic meter" && (
                  <>
                    <Text style={styles.textLabel}> 004 kWh meter reading</Text>
                    <TextInput
                      name="meterreading"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading")}
                      onBlur={handleBlur("meterreading")}
                      value={values.meterreading}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="Meter reading"
                    />
                    {errors.meterreading && (
                      <Text style={styles.error}>{errors.meterreading}</Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageOne(val)}
                    />
                    <Text style={styles.textLabel}>010 kWh meter reading</Text>
                    <TextInput
                      name="meterreading10"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading10")}
                      onBlur={handleBlur("meterreading10")}
                      value={values.meterreading10}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="010 meter reading"
                    />
                    {errors.meterreading10 && (
                      <Text style={styles.error}>{errors.meterreading10}</Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageTwo(val)}
                    />
                  </>
                )}

                {meterType === "three-phase" && (
                  <>
                    <Text style={styles.textLabel}>Your meter number 2</Text>
                    <TextInput
                      name="meternumber2"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meternumber2")}
                      onBlur={handleBlur("meternumber2")}
                      value={values.meternumber2}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="Meter number 2"
                    />
                    {errors.meternumber2 && (
                      <Text style={styles.error}>{errors.meternumber2}</Text>
                    )}
                    <Text style={styles.textLabel}>Your meter reading</Text>
                    <TextInput
                      name="meterreading2"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading2")}
                      onBlur={handleBlur("meterreading2")}
                      value={values.meterreading2}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="Meter reading"
                    />
                    {errors.meterreading2 && (
                      <Text style={styles.error}>{errors.meterreading2}</Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageTwo(val)}
                    />

                    <Text style={styles.textLabel}>Your meter number 3</Text>
                    <TextInput
                      name="meternumber3"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meternumber3")}
                      onBlur={handleBlur("meternumber3")}
                      value={values.meternumber3}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="Meter number 3"
                    />
                    {errors.meternumber3 && (
                      <Text style={styles.error}>{errors.meternumber3}</Text>
                    )}
                    <Text style={styles.textLabel}>Your meter reading</Text>
                    <TextInput
                      name="meterreading3"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading3")}
                      onBlur={handleBlur("meterreading3")}
                      value={values.meterreading3}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="Meter reading"
                    />
                    {errors.meterreading3 && (
                      <Text style={styles.error}>{errors.meterreading3}</Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageThree(val)}
                    />
                  </>
                )}
                <Text style={styles.textLabel}>Your phone number</Text>
                <TextInput
                  name="phone"
                  label=""
                  mode="outlined"
                  onChangeText={handleChange("phone")}
                  onBlur={handleBlur("phone")}
                  value={values.phone}
                  style={styles.textInput}
                  keyboardType="numeric"
                  maxLength={11}
                  accessibilityLabel="phone"
                />
                {errors.phone && (
                  <Text style={styles.error}>{errors.phone}</Text>
                )}
                <Text style={styles.textLabel}>Your email address</Text>
                <TextInput
                  name="email"
                  label=""
                  mode="outlined"
                  onChangeText={handleChange("email")}
                  onBlur={handleBlur("email")}
                  value={values.email}
                  style={styles.textInput}
                  keyboardType="email-address"
                  maxLength={100}
                  accessibilityLabel="email address"
                />
                {errors.email && touched.email ? (
                  <Text style={styles.error}>{errors.email}</Text>
                ) : null}
                <ConsentBox
                  name="acknowledgement"
                  onPress={(val) => {
                    setFieldValue("acknowledgement", val);
                  }}
                  value={values.acknowledgement}
                />
                {errors.acknowledgement && touched.acknowledgement ? (
                  <Text style={styles.error}>{errors.acknowledgement}</Text>
                ) : null}
                <TouchableOpacity
                  onPress={handleSubmit}
                  style={styles.footerContainer}
                >
                  <Text
                    style={{
                      color: "#ffffff",
                      fontSize: 20,
                    }}
                  >
                    Submit
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </Formik>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Style.GAP,
  },
  textLabel: {
    marginTop: Style.GAP,
    marginLeft: Style.GAP,
    fontSize: Style.FONT_SIZE_CARD,
    marginBottom: 2,
  },
  textInput: {
    margin: Style.GAP,
    marginTop: 0,
    fontSize: Style.FONT_SIZE_CARD,
  },
  dateInput: {
    flex: 1,
    margin: Style.GAP,
    marginRight: 0,
    marginTop: 0,
    fontSize: Style.FONT_SIZE_CARD,
  },
  yearInput: {
    flex: 2,
    margin: Style.GAP,
    marginTop: 0,
    fontSize: Style.FONT_SIZE_CARD,
  },

  footerContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    backgroundColor: "#2A3D6F",
    padding: Style.GAP * 0.6,
    alignItems: "center",
    color: "#fff",
    fontSize: 20,
    borderRadius: 4,
    fontFamily: "AzoSans-Regular",
    margin: Style.GAP,
  },
  customButton: {
    width: "50%",
    justifyContent: "flex-start",
    textAlign: "left",
    backgroundColor: "#ddd",
    height: 40,
  },
  thumbnail: {
    width: 100,
    height: 100,
    resizeMode: "contain",
  },
  error: {
    paddingLeft: Style.GAP,
    marginTop: 0,
    color: "red",
    fontSize: Style.FONT_SIZE_BODY,
  },
});
