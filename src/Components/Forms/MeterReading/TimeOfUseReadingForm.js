import React, { useState } from "react";
import {
  View,
  StyleSheet,
  Alert,
  Image,
  KeyboardAvoidingView,
} from "react-native";
import Header from "../../Header";
import { Headline } from "react-native-paper";
import { Formik } from "formik";
import * as yup from "yup";
import { Text, TextInput, Button, Modal, Portal } from "react-native-paper";
import * as Style from "../../styles/Style";
import RNDateTimePicker from "@react-native-community/datetimepicker";
import * as ImagePicker from "expo-image-picker";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { SubmitForm, SubmitTimeOfUseForm } from "../../../Utils/FormUtils";
import { useNavigation } from "@react-navigation/native";
import moment from "moment";

import OUTAGE_ICON from "../../../../assets/icons/calendar-alt-light-white.svg";

//FORM VALIDATION SCHEMA
const formValidationSchema = yup.object().shape({
  meternumber: yup.number().required("Meter number is required"),
  meterreading004: yup.number().required("Meter reading is required"),
  phone: yup.string().matches(/(0)(\d){9}\b/, "Enter a valid phone number"),
  email: yup
    .string()
    .email("please enter valid email")
    .required("Email address is required"),
});

//MAIN COMPONENT
export default function TimeOfUseReadingForm(props) {
  const { route } = props;
  const { meter } = route.params;
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [dateModalVisible, setDateModalVisible] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [singlePhaseButtonMode, setSinglePhaseButtonMode] =
    useState("contained");
  const [threePhaseButtonMode, setThreePhaseButtonMode] = useState("text");
  const [meterType, setMeterType] = useState("single-phase");

  // SINGLE AND THREEPHASE BUTTON TOGGLE
  //TODO: CREATE A SEPARATE REUSABLE COMPONENT FOR TOGGLE
  const onButtonToggle = (value) => {
    setSinglePhaseButtonMode(
      singlePhaseButtonMode === "contained" ? "text" : "contained"
    );
    setThreePhaseButtonMode(
      threePhaseButtonMode === "contained" ? "text" : "contained"
    );
    setMeterType(meterType === "single-phase" ? "three-phase" : "single-phase");
  };

  // Onchange: DATE
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    setDate(currentDate);
  };

  const showMode = () => {
    setShow(true);
  };

  // OPEN MODAL FOR PHOTO
  const showModal = () => setModalVisible(true);
  const hideModal = () => setModalVisible(false);

  // OPEN MODAL FOR PHOTO
  const showDateModal = () => setDateModalVisible(true);
  const hideDateModal = () => setDateModalVisible(false);

  const containerStyle = {
    backgroundColor: "#ffffff",
    padding: 20,
    margin: Style.GAP,
  };
  //ACCESS CAMERA
  let openCameraAsync = async () => {
    let permissionResult = await ImagePicker.requestCameraPermissionsAsync();

    if (permissionResult.granted === false) {
      Alert.alert("Permission to access camera roll is required!");
      return;
    }
    try {
      let cameraResult = await ImagePicker.launchCameraAsync();
      if (cameraResult.cancelled === true) {
        return;
      }
      setModalVisible(false);
      setSelectedImage({ localUri: cameraResult.uri });
    } catch (e) {}
  };

  //IMAGE PICKER METHODS
  let openImagePickerAsync = async () => {
    let permissionResult =
      await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      Alert.alert("Permission to access camera roll is required!");
      return;
    }
    try {
      let libraryResult = await ImagePicker.launchImageLibraryAsync();
      if (libraryResult.cancelled === true) {
        return;
      }
      setModalVisible(false);
      setSelectedImage({ localUri: libraryResult.uri });
    } catch (e) {
      console.log(e);
    }
  };

  //HANDLE FORM SUBMISSION
  const handlFormSubmit = async (
    meter,
    meterType,
    values,
    date,
    selectedImage
  ) => {
    let formId;
    if (meter === "Water Meter") {
      formId = 7;
    } else {
      formId = 8;
    }
    try {
      const result = await SubmitTimeOfUseForm(
        formId,
        meter,
        meterType,
        values,
        date,
        selectedImage
      );
      result === 200
        ? navigation.navigate("ThankYou", {
            msg: "Thank you for your submission.",
          })
        : Alert.alert("Error in submission. Please try again.");
    } catch (e) {
      Alert.alert(e);
    }
  };

  //RENDER MAIN SCREEN
  return (
    <>
      <Header title="Submit a meter read" />
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.container}
      >
        <Headline style={{ alignSelf: "center", marginTop: Style.GAP }}>
          {meter ? meter : ""}
        </Headline>
        <ScrollView>
          <Formik
            validationSchema={formValidationSchema}
            initialValues={{
              date: "",
              meternumber: "",
              meterreading004: "",
              meterreading010: "",
              meternumber2: "",
              meterreading0042: "",
              meterreading0102: "",
              meternumber3: "",
              meterreading0043: "",
              meterreading0103: "",
              phone: "",
              email: "",
            }}
            onSubmit={(values) =>
              handlFormSubmit(meter, meterType, values, date, selectedImage)
            }
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
              isValid,
            }) => (
              <View>
                {/* {show && (
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <RNDateTimePicker
                      value={date}
                      mode="date"
                      display="default"
                      onChange={onChange}
                      style={{
                        flex: 1,
                        alignContent: "center",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    />
                  </View>
                )} */}
                {modalVisible && (
                  <View>
                    <Portal>
                      <Modal
                        visible={modalVisible}
                        onDismiss={hideModal}
                        contentContainerStyle={containerStyle}
                      >
                        <Button
                          mode="outlined"
                          style={{
                            position: "relative",
                            alignItems: "center",
                            alignSelf: "flex-end",
                          }}
                          icon="close"
                          onPress={() => hideModal()}
                        />
                        <Button
                          mode="contained"
                          style={styles.footerContainer}
                          icon="camera"
                          onPress={openCameraAsync}
                        >
                          From Camera
                        </Button>
                        <Button
                          mode="contained"
                          style={styles.footerContainer}
                          icon="upload"
                          onPress={openImagePickerAsync}
                        >
                          Upload from Gallery
                        </Button>
                      </Modal>
                    </Portal>
                  </View>
                )}
                {dateModalVisible && (
                  <View>
                    <Portal>
                      <Modal
                        visible={dateModalVisible}
                        onDismiss={hideDateModal}
                        contentContainerStyle={containerStyle}
                      >
                        <RNDateTimePicker
                          value={date}
                          mode="date"
                          display="spinner"
                          onChange={onChange}
                          maximumDate={moment().toDate()}
                          minimumDate={moment().subtract(3, "days").toDate()}
                        />
                      </Modal>
                    </Portal>
                  </View>
                )}

                <View>
                  {/* {!show && (
                    <Button onPress={showMode}>Choose date of Reading</Button>
                  )} */}
                </View>
                {!(meter === "Water Meter") && (
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      margin: Style.GAP,
                      backgroundColor: "#EAEAEA",
                    }}
                  >
                    <Button
                      mode={singlePhaseButtonMode}
                      onPress={onButtonToggle}
                      uppercase={false}
                    >
                      single phase
                    </Button>
                    <Button
                      mode={threePhaseButtonMode}
                      onPress={onButtonToggle}
                      uppercase={false}
                    >
                      three phase
                    </Button>
                  </View>
                )}
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    alignItems: "center",
                    flexWrap: "nowrap",
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "#ddd",
                      padding: Style.GAP,
                      flex: 1,
                      alignContent: "center",
                      alignItems: "center",
                      justifyContent: "center",
                      margin: Style.GAP,
                    }}
                  >
                    {/* <TextInput
                    name="meternumber"
                    label={date.toDateString()}
                    mode="outlined"
                    onChangeText={handleChange("meternumber")}
                    onBlur={handleBlur("meternumber")}
                    value={values.meternumber}
                    style={({ flex: 1 }, styles.textInput)}
                    keyboardType="numeric"
                    inlineImageLeft="person"
                      onPress={showMode}
                        width="80%"
                        disabled
                      /> */}
                    <Text style={{ fontSize: Style.FONT_SIZE_HEADING }}>
                      {date.toDateString()}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={showDateModal}
                    style={{
                      flex: 1,
                      alignSelf: "center",
                      backgroundColor: Style.COLOR_PRIMARY,
                      padding: Style.GAP / 2,
                      justifyContent: "center",
                      margin: Style.GAP,
                      marginLeft: 0,
                    }}
                  >
                    <OUTAGE_ICON
                      width={36}
                      height={36}
                      fill={Style.COLOR_PRIMARY}
                    />
                  </TouchableOpacity>
                </View>
                <Text style={styles.textLabel}>Your meter number</Text>
                <TextInput
                  name="meternumber"
                  label="your meter number"
                  mode="outlined"
                  onChangeText={handleChange("meternumber")}
                  onBlur={handleBlur("meternumber")}
                  value={values.meternumber}
                  style={styles.textInput}
                  keyboardType={"numeric"}
                  maxLength={10}
                ></TextInput>
                {errors.meternumber && (
                  <Text style={styles.error}>{errors.meternumber}</Text>
                )}
                <Text style={styles.textLabel}>004 kWh Meter reading</Text>
                <TextInput
                  name="meterreading004"
                  label="Enter the reading"
                  mode="outlined"
                  onChangeText={handleChange("meterreading004")}
                  onBlur={handleBlur("meterreading004")}
                  value={values.meterreading004}
                  style={styles.textInput}
                  keyboardType="numeric"
                  maxLength={10}
                />
                {errors.meterreading004 && (
                  <Text style={styles.error}>{errors.meterreading004}</Text>
                )}
                <Text style={styles.textLabel}>010 kWh Meter reading</Text>
                <TextInput
                  name="meterreading010"
                  label="Enter the reading"
                  mode="outlined"
                  onChangeText={handleChange("meterreading010")}
                  onBlur={handleBlur("meterreading010")}
                  value={values.meterreading010}
                  style={styles.textInput}
                  keyboardType="numeric"
                  maxLength={10}
                />
                {errors.meterreading004 && (
                  <Text style={styles.error}>{errors.meterreading004}</Text>
                )}

                {meterType === "three-phase" && (
                  <>
                    <Text style={styles.textLabel}>Your meter number 2</Text>
                    <TextInput
                      name="meternumber2"
                      label="your meter number"
                      mode="outlined"
                      onChangeText={handleChange("meternumber2")}
                      onBlur={handleBlur("meternumber2")}
                      value={values.meternumber2}
                      style={styles.textInput}
                      keyboardType={
                        meter === "Water Meter" ? "default" : "numeric"
                      }
                      maxLength={10}
                    ></TextInput>
                    {errors.meternumber && (
                      <Text style={styles.error}>{errors.meternumber}</Text>
                    )}
                    <Text style={styles.textLabel}>
                      004 kWh Meter reading 2
                    </Text>
                    <TextInput
                      name="meterreading0042"
                      label="Enter the reading"
                      mode="outlined"
                      onChangeText={handleChange("meterreading0042")}
                      onBlur={handleBlur("meterreading0042")}
                      value={values.meterreading0042}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                    />
                    {errors.meterreading004 && (
                      <Text style={styles.error}>{errors.meterreading004}</Text>
                    )}
                    <Text style={styles.textLabel}>
                      010 kWh Meter reading 2
                    </Text>
                    <TextInput
                      name="meterreading0102"
                      label="Enter the reading"
                      mode="outlined"
                      onChangeText={handleChange("meterreading0102")}
                      onBlur={handleBlur("meterreading0102")}
                      value={values.meterreading0102}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                    />
                    {errors.meterreading004 && (
                      <Text style={styles.error}>{errors.meterreading004}</Text>
                    )}
                    <Text style={styles.textLabel}>Your meter number 3</Text>
                    <TextInput
                      name="meternumber3"
                      label="your meter number"
                      mode="outlined"
                      onChangeText={handleChange("meternumber3")}
                      onBlur={handleBlur("meternumber3")}
                      value={values.meternumber3}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                    ></TextInput>
                    {errors.meternumber && (
                      <Text style={styles.error}>{errors.meternumber}</Text>
                    )}
                    <Text style={styles.textLabel}>
                      004 kWh Meter reading 3
                    </Text>
                    <TextInput
                      name="meterreading0043"
                      label="Enter the reading"
                      mode="outlined"
                      onChangeText={handleChange("meterreading0043")}
                      onBlur={handleBlur("meterreading0043")}
                      value={values.meterreading0043}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                    />
                    {errors.meterreading004 && (
                      <Text style={styles.error}>{errors.meterreading004}</Text>
                    )}
                    <Text style={styles.textLabel}>
                      010 kWh Meter reading 3
                    </Text>
                    <TextInput
                      name="meterreading0103"
                      label="Enter the reading"
                      mode="outlined"
                      onChangeText={handleChange("meterreading0103")}
                      onBlur={handleBlur("meterreading0103")}
                      value={values.meterreading}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                    />
                    {errors.meterreading004 && (
                      <Text style={styles.error}>{errors.meterreading004}</Text>
                    )}
                  </>
                )}
                <Text style={styles.textLabel}>Your phone number</Text>
                <TextInput
                  name="phone"
                  label="(ex: 0400111000)"
                  mode="outlined"
                  onChangeText={handleChange("phone")}
                  onBlur={handleBlur("phone")}
                  value={values.phone}
                  style={styles.textInput}
                  keyboardType="numeric"
                  maxLength={10}
                />
                {errors.phone && (
                  <Text style={styles.error}>{errors.phone}</Text>
                )}
                <Text style={styles.textLabel}>Your email address</Text>
                <TextInput
                  name="email"
                  label="(ex: your@email.com)"
                  mode="outlined"
                  onChangeText={handleChange("email")}
                  onBlur={handleBlur("email")}
                  value={values.email}
                  style={styles.textInput}
                  keyboardType="email-address"
                  maxLength={100}
                />
                {errors.email && (
                  <Text style={styles.error}>{errors.email}</Text>
                )}
                <View style={styles.footerContainer}>
                  <Button
                    mode="contained"
                    icon="camera"
                    uppercase={false}
                    onPress={showModal}
                  >
                    Upload Photo
                  </Button>
                </View>
                {selectedImage && (
                  <View style={styles.container}>
                    <Image
                      source={{ uri: selectedImage.localUri }}
                      style={styles.thumbnail}
                    />
                  </View>
                )}
                <TouchableOpacity
                  onPress={handleSubmit}
                  style={styles.footerContainer}
                >
                  <Text
                    style={{
                      color: "#ffffff",
                      fontSize: 20,
                    }}
                  >
                    Submit
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </Formik>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Style.GAP,
  },
  textLabel: {
    marginTop: Style.GAP,
    marginLeft: Style.GAP,
    fontSize: Style.FONT_SIZE_CARD,
  },
  textInput: {
    margin: Style.GAP,
    marginTop: 0,
    fontSize: Style.FONT_SIZE_CARD,
  },
  footerContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    backgroundColor: "#2A3D6F",
    padding: Style.GAP * 0.6,
    alignItems: "center",
    color: "#fff",
    fontSize: 20,
    borderRadius: 4,
    fontFamily: "AzoSans-Regular",
    margin: Style.GAP,
  },
  thumbnail: {
    width: 100,
    height: 100,
    resizeMode: "contain",
  },
  error: {
    paddingLeft: Style.GAP,
    marginTop: 0,
    color: "red",
    fontSize: Style.FONT_SIZE_BODY,
  },
});
