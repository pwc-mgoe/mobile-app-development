import React, { useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  Alert,
  Image,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import Header from "../../Header";
import { Formik } from "formik";
import * as yup from "yup";
import {
  Text,
  TextInput,
  Button,
  Modal,
  Portal,
  Headline,
  Checkbox,
  ActivityIndicator,
} from "react-native-paper";
import * as Style from "../../styles/Style";
import RNDateTimePicker from "@react-native-community/datetimepicker";
import * as ImagePicker from "expo-image-picker";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { SubmitSolarForm } from "../../../Utils/FormUtils";
import { useNavigation } from "@react-navigation/native";
import moment from "moment";
import OUTAGE_ICON from "../../../../assets/icons/calendar-alt-light-white.svg";
import ConsentBox from "../../ConsentBox";
import PhotoPicker from "../../PhotoPicker";
import {
  ElectronicValidation,
  PowerSinglePhaseValidation,
  PowerThreePhaseValidation,
  SolarCombinationValidation,
  SolarSinglePhaseValidation,
  SolarThreePhaseValidation,
  WaterValidation,
} from "../Validation/FormValidation";
import DatePicker, { getFormatedDate } from "react-native-modern-datepicker";

//MAIN COMPONENT
export default function SolarReadingForm({ route }) {
  const { meter } = route.params;
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [dateModalVisible, setDateModalVisible] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);
  const [date, setDate] = useState(new Date());
  const [selectedDate, setSelectedDate] = useState(new Date());

  const [show, setShow] = useState(false);
  const [singlePhaseButtonMode, setSinglePhaseButtonMode] =
    useState("contained");
  const [checked, setChecked] = React.useState(false);
  const [threePhaseButtonMode, setThreePhaseButtonMode] = useState("text");
  const [combinationButtonMode, setCombinationButtonMode] = useState("text");
  const [meterType, setMeterType] = useState("single-phase");
  const [privacyCheckboxStatus, setPrivacyCheckboxStatus] = useState(null);
  const [loading, setLoading] = useState(false);

  const [selectedImageOne, setSelectedImageOne] = useState([]);
  const [selectedImageTwo, setSelectedImageTwo] = useState([]);
  const [selectedImageThree, setSelectedImageThree] = useState([]);
  const [selectedImageFour, setSelectedImageFour] = useState([]);
  const [selectedImageFive, setSelectedImageFive] = useState([]);
  const [selectedImageSix, setSelectedImageSix] = useState([]);
  const [formValidationSchema, setFormValidationSchema] = useState();

  // SINGLE AND THREEPHASE BUTTON TOGGLE
  //TODO: CREATE A SEPARATE REUSABLE COMPONENT FOR TOGGLE
  const onButtonToggle = (solarMeterType) => {
    if (solarMeterType === "single-phase") {
      setSinglePhaseButtonMode("contained");
      setThreePhaseButtonMode("text");
      setCombinationButtonMode("text");
      setMeterType("single-phase");
    } else if (solarMeterType === "three-phase") {
      setSinglePhaseButtonMode("text");
      setThreePhaseButtonMode("contained");
      setCombinationButtonMode("text");
      setMeterType("three-phase");
    } else {
      setSinglePhaseButtonMode("text");
      setThreePhaseButtonMode("text");
      setCombinationButtonMode("contained");
      setMeterType("combination");
    }
  };

  useEffect(() => {
    if (meter == "Solar meter" && meterType === "single-phase") {
      setFormValidationSchema(SolarSinglePhaseValidation);
    } else if (meter == "Solar meter" && meterType === "three-phase") {
      setFormValidationSchema(SolarThreePhaseValidation);
    } else if (meter == "Solar meter" && meterType === "combination") {
      setFormValidationSchema(SolarCombinationValidation);
    }
  });

  // Onchange: DATE
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    setDate(currentDate);
  };

  const showMode = () => {
    setShow(true);
  };

  // OPEN MODAL FOR PHOTO
  const showModal = () => setModalVisible(true);
  const hideModal = () => setModalVisible(false);

  // OPEN MODAL FOR PHOTO
  const showDateModal = () => setDateModalVisible(true);
  const hideDateModal = () => setDateModalVisible(false);

  const containerStyle = {
    backgroundColor: "#ffffff",
    padding: 20,
    margin: Style.GAP,
  };

  const prepareImageSubmission = () => {
    let imageBundle = [];
    selectedImageOne?.map((image) => imageBundle.push(image.base64Uri));
    selectedImageTwo?.map((image) => imageBundle.push(image.base64Uri));
    selectedImageThree?.map((image) => imageBundle.push(image.base64Uri));
    selectedImageFour?.map((image) => imageBundle.push(image.base64Uri));
    selectedImageFive?.map((image) => imageBundle.push(image.base64Uri));
    selectedImageSix?.map((image) => imageBundle.push(image.base64Uri));

    return imageBundle;
  };

  //HANDLE FORM SUBMISSION
  const handlFormSubmit = async (
    meter,
    meterType,
    values,
    date,
    selectedImage
  ) => {
    let formId;
    if (meter === "Water Meter") {
      formId = 7;
    } else {
      formId = 8;
    }

    try {
      setLoading(true);
      const imageBundle = prepareImageSubmission();
      const result = await SubmitSolarForm(
        formId,
        meter,
        meterType,
        values,
        selectedDate,
        imageBundle
      );
      setLoading(false);

      result === 200
        ? navigation.navigate("ThankYou", {
            msg: "Thank you for your submission.",
          })
        : Alert.alert("Error in submission. Please try again.");
    } catch (e) {
      Alert.alert(e);
    }
  };

  const handlePressPrivacyCheckboxStatus = (value) => {
    setPrivacyCheckboxStatus(value);
  };

  //RENDER MAIN SCREEN
  return (
    <>
      <Header title="Submit a meter read" />
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.container}
      >
        <Headline style={{ alignSelf: "center", marginTop: Style.GAP }}>
          {meter ? meter : ""}
        </Headline>
        <ScrollView>
          {loading && (
            <View>
              <Portal>
                <Modal
                  visible={loading}
                  style={{ opacity: 1 }}
                  contentContainerStyle={containerStyle}
                  onDismiss={() => {}}
                >
                  <ActivityIndicator animating={true} size="large" />
                  <Headline style={{ textAlign: "center", margin: Style.GAP }}>
                    Submission in progress. Please hold..
                  </Headline>
                </Modal>
              </Portal>
            </View>
          )}
          <Formik
            validationSchema={formValidationSchema}
            initialValues={{
              day: moment(selectedDate).format("DD"),
              month: moment(selectedDate).format("MM"),
              year: moment(selectedDate).format("yyyy"),
              date: "",
              meternumber: "",
              meterreading003: "",
              meternumber2: "",
              meterreading0032: "",
              meterreading0232: "",
              meternumber3: "",
              meterreading0033: "",
              meterreading0233: "",
              meterreading04: "",
              meterreading010: "",
              meterreading023: "",
              phone: "",
              email: "",
              acknowledgement: "",
            }}
            onSubmit={(values) =>
              handlFormSubmit(meter, meterType, values, date, selectedImage)
            }
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
              touched,
              setFieldValue,
            }) => (
              <View>
                {dateModalVisible && (
                  <View>
                    <Portal>
                      <Modal
                        visible={dateModalVisible}
                        onDismiss={() => hideDateModal()}
                        contentContainerStyle={containerStyle}
                      >
                        <Button
                          mode="outlined"
                          style={{
                            position: "relative",
                            alignItems: "center",
                            alignSelf: "flex-end",
                          }}
                          icon="close"
                          onPress={() => hideDateModal()}
                        />
                        <DatePicker
                          options={{
                            defaultFont: "AzoSans-Regular",
                            headerFont: "AzoSans-Regular",
                            textDefaultColor: "#333333",
                            backgroundColor: "#ffffff",
                            selectedTextColor: "#ffffff",
                            mainColor: "#2A3D6F",
                          }}
                          mode="calendar"
                          display="date"
                          maximumDate={moment().format("YYYY/MM/DD")}
                          minimumDate={moment()
                            .subtract(3, "days")
                            .format("YYYY/MM/DD")
                            .toString()}
                          onSelectedChange={(date) => {
                            setSelectedDate(date);
                          }}
                          selected={getFormatedDate(new Date(), "YYYY/MM/DD")}
                        />
                      </Modal>
                    </Portal>
                  </View>
                )}
                {!(meter === "Water Meter") && (
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      margin: Style.GAP,
                      backgroundColor: "#EAEAEA",
                    }}
                  >
                    <Button
                      mode={singlePhaseButtonMode}
                      onPress={() => onButtonToggle("single-phase")}
                      uppercase={false}
                    >
                      Single phase
                    </Button>
                    <Button
                      mode={threePhaseButtonMode}
                      onPress={() => onButtonToggle("three-phase")}
                      uppercase={false}
                    >
                      3 phase
                    </Button>
                    <Button
                      mode={combinationButtonMode}
                      onPress={() => onButtonToggle("combination")}
                      uppercase={false}
                    >
                      Combination
                    </Button>
                  </View>
                )}
                <Text style={styles.textLabel}>Date of reading</Text>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <View style={{ flexDirection: "column", flex: 1 }}>
                    <TextInput
                      name="day"
                      label="dd"
                      mode="outlined"
                      onChangeText={handleChange("day")}
                      onBlur={handleBlur("day")}
                      value={values.day}
                      style={styles.dateInput}
                      maxLength={2}
                      accessibilityLabel="day"
                      keyboardType="numeric"
                    />
                    {errors.day && touched.day ? (
                      <Text style={styles.error}>{errors.day}</Text>
                    ) : null}
                  </View>
                  <View style={{ flexDirection: "column", flex: 1 }}>
                    <TextInput
                      name="month"
                      label="mm"
                      mode="outlined"
                      onChangeText={handleChange("month")}
                      onBlur={handleBlur("month")}
                      value={values.month}
                      keyboardType="numeric"
                      maxLength={2}
                      style={styles.dateInput}
                      accessibilityLabel="month"
                    />
                    {errors.month && touched.month ? (
                      <Text style={styles.error}>{errors.month}</Text>
                    ) : null}
                  </View>
                  <View style={{ flexDirection: "column", flex: 2 }}>
                    <TextInput
                      name="year"
                      label="yyyy"
                      mode="outlined"
                      onChangeText={handleChange("year")}
                      onBlur={handleBlur("year")}
                      value={values.year}
                      style={styles.yearInput}
                      keyboardType="numeric"
                      maxLength={4}
                      accessibilityLabel="year"
                    />
                    {errors.year && touched.year ? (
                      <Text style={styles.error}>{errors.year}</Text>
                    ) : null}
                  </View>
                </View>
                {meterType === "three-phase" ? (
                  <Text style={styles.textLabel}>Your meter number 1</Text>
                ) : (
                  <Text style={styles.textLabel}>Your meter number</Text>
                )}
                <TextInput
                  name="meternumber"
                  label=""
                  mode="outlined"
                  onChangeText={handleChange("meternumber")}
                  onBlur={handleBlur("meternumber")}
                  value={values.meternumber}
                  style={styles.textInput}
                  keyboardType={"numeric"}
                  maxLength={10}
                  accessibilityLabel="Meter number"
                ></TextInput>
                {errors.meternumber && (
                  <Text style={styles.error}>{errors.meternumber}</Text>
                )}
                {meterType !== "combination" && (
                  <>
                    {meterType === "three-phase" ? (
                      <Text style={styles.textLabel}>
                        003 kWh meter reading
                      </Text>
                    ) : (
                      <Text style={styles.textLabel}>
                        003 kWh meter reading
                      </Text>
                    )}
                    <TextInput
                      name="meterreading003"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading003")}
                      onBlur={handleBlur("meterreading003")}
                      value={values.meterreading003}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="Meter reading 3"
                    />
                    {errors.meterreading003 && (
                      <Text style={styles.error}>{errors.meterreading003}</Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageOne(val)}
                    />
                  </>
                )}
                {meterType === "combination" && (
                  <>
                    <Text style={styles.textLabel}>004 kWh meter reading</Text>
                    <TextInput
                      name="meterreading04"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading04")}
                      onBlur={handleBlur("meterreading04")}
                      value={values.meterreading04}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="004 meter reading"
                    />
                    {errors.meterreading04 && (
                      <Text style={styles.error}>{errors.meterreading04}</Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageOne(val)}
                    />
                    <Text style={styles.textLabel}>010 kWh meter reading</Text>
                    <TextInput
                      name="meterreading010"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading010")}
                      onBlur={handleBlur("meterreading010")}
                      value={values.meterreading010}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="010 meter reading"
                    />
                    {errors.meterreading010 && (
                      <Text style={styles.error}>{errors.meterreading010}</Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageThree(val)}
                    />
                  </>
                )}
                <Text style={styles.textLabel}>023 kWh meter reading</Text>
                <TextInput
                  name="meterreading023"
                  label=""
                  mode="outlined"
                  onChangeText={handleChange("meterreading023")}
                  onBlur={handleBlur("meterreading023")}
                  value={values.meterreading023}
                  style={styles.textInput}
                  keyboardType="numeric"
                  maxLength={10}
                  accessibilityLabel="023 meter reading"
                />
                {errors.meterreading023 && (
                  <Text style={styles.error}>{errors.meterreading023}</Text>
                )}
                <PhotoPicker
                  numberOfImages={1}
                  onChange={(val) => setSelectedImageTwo(val)}
                />
                {meterType === "three-phase" && (
                  <>
                    <Text style={styles.textLabel}>Your meter number 2</Text>
                    <TextInput
                      name="meternumber2"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meternumber2")}
                      onBlur={handleBlur("meternumber2")}
                      value={values.meternumber2}
                      style={styles.textInput}
                      keyboardType={
                        meter === "Water Meter" ? "default" : "numeric"
                      }
                      maxLength={10}
                      accessibilityLabel="Meter number 2"
                    />
                    {errors.meternumber2 && (
                      <Text style={styles.error}>{errors.meternumber2}</Text>
                    )}
                    <Text style={styles.textLabel}>003 kWh meter reading</Text>
                    <TextInput
                      name="meterreading0032"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading0032")}
                      onBlur={handleBlur("meterreading0032")}
                      value={values.meterreading0032}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="003 meter reading 2"
                    />
                    {errors.meterreading0032 && (
                      <Text style={styles.error}>
                        {errors.meterreading0032}
                      </Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageThree(val)}
                    />
                    <Text style={styles.textLabel}>023 kWh meter reading</Text>
                    <TextInput
                      name="meterreading0232"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading0232")}
                      onBlur={handleBlur("meterreading0232")}
                      value={values.meterreading0232}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="023 meter reading 2"
                    />
                    {errors.meterreading0232 && (
                      <Text style={styles.error}>
                        {errors.meterreading0232}
                      </Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageFour(val)}
                    />
                    <Text style={styles.textLabel}>Your meter number 3</Text>
                    <TextInput
                      name="meternumber3"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meternumber3")}
                      onBlur={handleBlur("meternumber3")}
                      value={values.meternumber3}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel=" meter number 3"
                    />
                    {errors.meternumber3 && (
                      <Text style={styles.error}>{errors.meternumber3}</Text>
                    )}
                    <Text style={styles.textLabel}>003 kWh meter reading</Text>
                    <TextInput
                      name="meterreading0033"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading0033")}
                      onBlur={handleBlur("meterreading0033")}
                      value={values.meterreading0033}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="003 meter reading 3"
                    />
                    {errors.meterreading0033 && (
                      <Text style={styles.error}>
                        {errors.meterreading0033}
                      </Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageFive(val)}
                    />
                    <Text style={styles.textLabel}>023 kWh meter reading</Text>
                    <TextInput
                      name="meterreading0233"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("meterreading0233")}
                      onBlur={handleBlur("meterreading0233")}
                      value={values.meterreading0233}
                      style={styles.textInput}
                      keyboardType="numeric"
                      maxLength={10}
                      accessibilityLabel="023 meter reading 3"
                    />
                    {errors.meterreading0233 && (
                      <Text style={styles.error}>
                        {errors.meterreading0233}
                      </Text>
                    )}
                    <PhotoPicker
                      numberOfImages={1}
                      onChange={(val) => setSelectedImageSix(val)}
                    />
                  </>
                )}

                <Text style={styles.textLabel}>Your phone number</Text>
                <TextInput
                  name="phone"
                  label=""
                  mode="outlined"
                  onChangeText={handleChange("phone")}
                  onBlur={handleBlur("phone")}
                  value={values.phone}
                  style={styles.textInput}
                  keyboardType="numeric"
                  maxLength={11}
                  accessibilityLabel="phone number"
                />
                {errors.phone && (
                  <Text style={styles.error}>{errors.phone}</Text>
                )}
                <Text style={styles.textLabel}>Your email address</Text>
                <TextInput
                  name="email"
                  label=""
                  mode="outlined"
                  onChangeText={handleChange("email")}
                  onBlur={handleBlur("email")}
                  value={values.email}
                  style={styles.textInput}
                  keyboardType="email-address"
                  maxLength={100}
                  accessibilityLabel="email address"
                />
                {errors.email && (
                  <Text style={styles.error}>{errors.email}</Text>
                )}
                <ConsentBox
                  name="acknowledgement"
                  onPress={(val) => {
                    setFieldValue("acknowledgement", val);
                  }}
                  value={values.acknowledgement}
                />
                {errors.acknowledgement && touched.acknowledgement ? (
                  <Text style={styles.error}>{errors.acknowledgement}</Text>
                ) : null}

                <TouchableOpacity
                  onPress={handleSubmit}
                  style={styles.footerContainer}
                >
                  <Text
                    style={{
                      color: "#ffffff",
                      fontSize: 20,
                    }}
                  >
                    Submit
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </Formik>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Style.GAP,
  },
  textLabel: {
    marginTop: Style.GAP,
    marginLeft: Style.GAP,
    fontSize: Style.FONT_SIZE_CARD,
    marginBottom: 2,
  },
  textInput: {
    margin: Style.GAP,
    marginTop: 0,
    fontSize: Style.FONT_SIZE_CARD,
  },
  dateInput: {
    flex: 1,
    margin: Style.GAP,
    marginRight: 0,
    marginTop: 0,
    fontSize: Style.FONT_SIZE_CARD,
  },
  yearInput: {
    flex: 2,
    margin: Style.GAP,
    marginTop: 0,
    fontSize: Style.FONT_SIZE_CARD,
  },

  footerContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    backgroundColor: "#2A3D6F",
    padding: Style.GAP * 0.6,
    alignItems: "center",
    color: "#fff",
    fontSize: 20,
    borderRadius: 4,
    fontFamily: "AzoSans-Regular",
    margin: Style.GAP,
  },
  photoButtonContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#ffffff",
    padding: Style.GAP * 0.1,
    paddingLeft: 0,
    color: "#fff",
    fontSize: 20,
    borderRadius: 2,
    margin: Style.GAP,
    marginTop: 0,
    borderColor: "#333",
    borderWidth: 1,
  },
  customButton: {
    width: "50%",
    justifyContent: "flex-start",
    textAlign: "left",
    backgroundColor: "#ddd",
    height: 40,
  },
  thumbnail: {
    width: 100,
    height: 100,
    resizeMode: "contain",
  },
  error: {
    paddingLeft: Style.GAP,
    marginTop: 0,
    color: "red",
    fontSize: Style.FONT_SIZE_BODY,
  },
});
