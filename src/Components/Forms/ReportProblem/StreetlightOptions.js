import React, { useState, useEffect } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import Header from "../../Header";
import { Button, Headline } from "react-native-paper";
import * as Style from "../../styles/Style";
import Accordion from "../../Accordion";
import ReportProblemForm from "./ReportProblemForm";

const streetlightOptions = require("./options/StreetlightOptions.json");

export default function StreetlightOptions() {
  const [showEmergencyMessage, setShowEmergencyMessage] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [levelOne, setLevelOne] = useState(null);
  const [levelTwo, setLevelTwo] = useState(null);
  const [levelThree, setLevelThree] = useState(null);
  const [levelFour, setLevelFour] = useState(null);
  const [formMessage, setFormMessage] = useState([]);

  //Actions when the first accordion is pressed
  useEffect(() => {
    setLevelOne(streetlightOptions);
  }, [levelOne]);

  function _resetFields() {
    setLevelOne(null);
    setLevelTwo(null);
    setLevelThree(null);
    setLevelFour(null);
    setShowEmergencyMessage(false);
    setShowForm(false);
    setFormMessage([]);
  }

  function onFirstLevelPressed(value) {
    setShowForm(false);
    setLevelTwo(null);
    setLevelThree(null);
    setFormMessage([...formMessage, (value.value || value).toString()]);

    if (!value.children) {
      setShowForm(true);
    } else if (value.displayMessage) {
      setShowEmergencyMessage(true);
    } else if (!(value.name === "other") || value.children) {
      setLevelTwo(value);
    } else {
      setShowForm(true);
    }
  }

  //actions when second accordion is pressed
  const onSecondLevelPressed = (value) => {
    setShowForm(false);
    setLevelThree(null);
    setFormMessage([...formMessage, (value?.value || value).toString()]);

    if (!value.children) {
      setShowForm(true);
    } else if (value.displayMessage) {
      setShowEmergencyMessage(true);
    } else if (!(value.name === "other") || value.children) {
      setLevelThree(value);
    } else {
      setShowEmergencyMessage(false);
      setShowForm(true);
    }
  };

  //actions when third accordion is pressed
  const onThirdLevelPressed = (value) => {
    setLevelFour(null);
    setShowForm(false);

    setFormMessage([...formMessage, (value?.value || value).toString()]);

    if (value.displayMessage) {
      setShowEmergencyMessage(true);
    } else if (!value.children) {
      setShowEmergencyMessage(false);
      setShowForm(true);
    } else if (!(value.name === "other" && value.children)) {
      setLevelFour(value);
    } else {
      setShowEmergencyMessage(false);
      setShowForm(true);
    }
    setLevelFour(value);
  };

  function _getFaultValue() {
    return formMessage.join(" / ");
  }

  return (
    <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
      <Header title={"Report streetlight fault"} />
      <ScrollView
        style={{ margin: Style.GAP, backgroundColor: Style.COLOR_WHITE }}
      >
        <Button
          uppercase={false}
          mode={"contained"}
          accessibilityLabel={"Start again"}
          onPress={() => _resetFields()}
        >
          Start again
        </Button>
        {levelOne && (
          <Accordion
            list={streetlightOptions.children}
            headline={streetlightOptions.headline}
            onItemPressed={onFirstLevelPressed}
          />
        )}

        {levelTwo && (
          <>
            <Accordion
              headline={levelTwo.headline}
              list={levelTwo.children}
              onItemPressed={onSecondLevelPressed}
            />
          </>
        )}
        {levelThree && (
          <>
            <Accordion
              list={levelThree.children}
              headline={levelThree.headline}
              onItemPressed={onThirdLevelPressed}
            />
          </>
        )}
        {showEmergencyMessage && (
          <Headline style={styles.emergencyMessage}>
            Don't touch fallen powerlines, keep others away and immediately call
            000 or Power and Water's emergency line 1800 245 090
          </Headline>
        )}
        {showForm && (
          <ReportProblemForm parent={"streetlight"} fault={_getFaultValue()} />
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  headline: {
    marginTop: Style.GAP * 2,
  },
  emergencyMessage: {
    textAlign: "center",
    padding: Style.GAP,
  },
});
