import React, { forwardRef, useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Linking,
  TouchableOpacity,
  TouchableHighlight,
} from "react-native";
import Header from "../../Header";
import { Button, Headline, Text, List, TextInput } from "react-native-paper";
import * as Style from "../../styles/Style";
import Accordion from "../../Accordion";
import ReportProblemForm from "./ReportProblemForm";

const powerOptions = require("./options/PowerOptions.json");

export default function PowerOptions(props) {
  const [showEmergencyMessage, setShowEmergencyMessage] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [levelOne, setLevelOne] = useState(null);
  const [levelTwo, setLevelTwo] = useState(null);
  const [levelThree, setLevelThree] = useState(null);
  const [levelFour, setLevelFour] = useState(null);
  const [formMessage, setFormMessage] = useState([]);

  //Actions when the first accordion is pressed
  useEffect(() => {
    setLevelOne(powerOptions);
  }, [levelOne]);

  function _resetFields() {
    setLevelOne(null);
    setLevelTwo(null);
    setLevelThree(null);
    setLevelFour(null);
    setShowEmergencyMessage(false);
    setShowForm(false);
    setFormMessage([]);
  }

  function onFirstLevelPressed(value) {
    setShowForm(false);
    setLevelTwo(null);
    setLevelThree(null);
    setFormMessage([...formMessage, (value.value || value).toString()]);

    if (!value.children) {
      setShowForm(true);
    } else if (value.displayMessage) {
      setShowEmergencyMessage(true);
    } else if (!(value.name === "other") || value.children) {
      setLevelTwo(value);
    } else {
      setShowForm(true);
    }
  }

  //actions when second accordion is pressed
  const onSecondLevelPressed = (value) => {
    setShowForm(false);
    setLevelThree(null);
    setFormMessage([...formMessage, (value?.value || value).toString()]);

    if (!value.children) {
      setShowForm(true);
    } else if (value.displayMessage) {
      setShowEmergencyMessage(true);
    } else if (!(value.name === "other") || value.children) {
      setLevelThree(value);
    } else {
      setShowEmergencyMessage(false);
      setShowForm(true);
    }
  };

  //actions when third accordion is pressed
  const onThirdLevelPressed = (value) => {
    setLevelFour(null);
    setShowForm(false);

    setFormMessage([...formMessage, (value?.value || value).toString()]);

    if (value.displayMessage) {
      setShowEmergencyMessage(true);
    } else if (!value.children) {
      setShowEmergencyMessage(false);
      setShowForm(true);
    } else if (!(value.name === "other" && value.children)) {
      setLevelFour(value);
    } else {
      setShowEmergencyMessage(false);
      setShowForm(true);
    }
    setLevelFour(value);
  };

  function _getFaultValue() {
    return formMessage.join(" / ");
  }

  async function _handleBlockPress(url) {
    (await Linking.canOpenURL(url))
      ? Linking.openURL(url)
      : Linking.openURL(
          "https://www.powerwater.com.au/customers/online-services/contact-us"
        );
  }

  return (
    <>
      <Header title={"Report power fault"} />
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        // style={styles.container}
        keyboardVerticalOffset={Platform.select({
          ios: Style.GAP,
          android: Style.GAP,
        })}
        style={{ flex: 1, backgroundColor: "#ffffff" }}
      >
        <ScrollView
          style={{ margin: Style.GAP, backgroundColor: Style.COLOR_WHITE }}
        >
          <Button
            uppercase={false}
            mode="contained"
            onPress={() => _resetFields()}
            accessibilityLabel={"Start Again"}
          >
            Start again
          </Button>
          {levelOne && (
            <Accordion
              list={powerOptions.children}
              headline={powerOptions.headline}
              onItemPressed={onFirstLevelPressed}
            />
          )}

          {levelTwo && (
            <>
              <Accordion
                headline={levelTwo.headline}
                list={levelTwo.children}
                onItemPressed={onSecondLevelPressed}
              />
            </>
          )}
          {levelThree && (
            <>
              <Accordion
                list={levelThree.children}
                headline={levelThree.headline}
                onItemPressed={onThirdLevelPressed}
              />
            </>
          )}
          {showEmergencyMessage && (
            <Headline style={styles.emergencyMessage}>
              Don't touch fallen powerlines, keep others away and immediately
              call {` `}
              <Text
                style={{ color: Style.COLOR_PRIMARY }}
                onPress={() => _handleBlockPress("tel:000")}
              >
                000{` `}
              </Text>
              or the Power and Water emergency line on
              <Text
                style={{ color: Style.COLOR_PRIMARY }}
                onPress={() => _handleBlockPress("tel:1800245090")}
              >
                {" "}
                1800 245 090
              </Text>
              {` `}
              immediately.
            </Headline>
          )}
          {showForm && (
            <ReportProblemForm parent={"power"} fault={_getFaultValue()} />
          )}
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
}

const styles = StyleSheet.create({
  headline: {
    marginTop: Style.GAP * 2,
  },
  emergencyMessage: {
    textAlign: "center",
    padding: Style.GAP,
  },
});
