import React, { useState, useEffect } from "react";
import { View, StyleSheet, Alert, Dimensions } from "react-native";
import { Formik, useFormikContext } from "formik";
import * as yup from "yup";
import {
  Text,
  TextInput,
  Modal,
  Portal,
  ActivityIndicator,
  Headline,
  RadioButton,
} from "react-native-paper";
import * as Style from "../../styles/Style";
import { TouchableOpacity } from "react-native-gesture-handler";
import { SubmitReportFaultForm } from "../../../Utils/ReportFaultFormUtils";
import { useNavigation } from "@react-navigation/native";
import Map from "../../Map";
import ConsentBox from "../../ConsentBox";
import PhotoPicker from "../../PhotoPicker";
import { ReportFaultValidation } from "../Validation/FormValidation";

//MAIN COMPONENT
export default function ReportProblemForm(props) {
  const { fault, parent } = props;
  const navigation = useNavigation();
  const [selectedImage, setSelectedImage] = useState(null);
  const [date, setDate] = useState(new Date());
  const [location, setLocation] = useState(null);
  const [pinAddress, setPinAddress] = useState("N/A");
  const [privacyCheckboxStatus, setPrivacyCheckboxStatus] = useState(false);
  const [loading, setLoading] = useState(false);
  const [value, setValue] = React.useState(null);

  function handleCoordinatesChange(value) {
    const { coordinate } = value.nativeEvent;
    setLocation(coordinate);
  }

  function handleAddressChange(value) {
    setPinAddress(value);
  }

  const containerStyle = {
    backgroundColor: "white",
    padding: 20,
    margin: Style.GAP,
  };

  const prepareImageSubmission = () => {
    let imageBundle = [];
    selectedImage?.map((image) => imageBundle.push(image.base64Uri));

    return imageBundle;
  };

  //HANDLE FORM SUBMISSION
  const handlFormSubmit = async (values) => {
    // Different form ID's
    let formId;
    // console.warn(parent);
    if (parent === "streetlight") {
      formId = 3; //used from meter reading form
    } else if (parent === "water") {
      formId = 4;
    } else if (parent === "sewerage") {
      formId = 5;
    } else {
      formId = 15; //for Power fault
    }

    try {
      const imageBundle = prepareImageSubmission();

      ///set address value from outside as it's not being set in the form "
      //TODO: change the value inside the form itself
      values.address = pinAddress;
      setLoading(true);
      const result = await SubmitReportFaultForm(
        formId,
        fault,
        values,
        imageBundle
      );
      setLoading(false);
      result === 200
        ? navigation.navigate("ThankYou", {
            msg:
              "Thank you for your submission." +
              "\n" +
              " We will process your online report during business hours, 8am to 4.30pm, Monday to Friday.\n" +
              "\n" +
              "To report an outage outside of these hours please call 1800 245 090.\n" +
              "\n",
          })
        : Alert.alert("Error in submission. Please try again.");
    } catch (e) {
      Alert.alert(e);
    }
  };

  //RENDER MAIN SCREEN
  return (
    <>
      {loading && (
        <View>
          <Portal>
            <Modal
              visible={loading}
              style={{ opacity: 1 }}
              contentContainerStyle={styles.containerStyle}
              onDismiss={() => {}}
            >
              <ActivityIndicator animating={true} size="large" />
              <Headline style={{ textAlign: "center", margin: Style.GAP }}>
                Submission in progress. Please hold..
              </Headline>
            </Modal>
          </Portal>
        </View>
      )}
      <Formik
        validationSchema={ReportFaultValidation}
        initialValues={{
          date: date ? date : "",
          streetlight: parent === "streetlight" ? true : false,
          serial: "",
          address: pinAddress ? pinAddress : "",
          // address: "",
          phone: "",
          email: "",
          comment: "",
          accessIssue: "",
          acknowledgement: "",
          privacy: privacyCheckboxStatus
            ? privacyCheckboxStatus
            : !privacyCheckboxStatus,
        }}
        onSubmit={(values) => handlFormSubmit(values, date, selectedImage)}
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue,
          values,
          errors,
          touched,
        }) => (
          <View>
            <View></View>
            <>
              {parent === "streetlight" && (
                <>
                  <Text style={styles.textLabel}>Serial Number</Text>
                  <TextInput
                    name="serial"
                    label=""
                    mode="outlined"
                    onChangeText={handleChange("serial")}
                    onBlur={handleBlur("serial")}
                    value={values.serial}
                    style={styles.textInput}
                    keyboardType="numeric"
                    maxLength={10}
                  />
                  {errors.serial && touched.serial ? (
                    <Text style={styles.error}>{errors.serial}</Text>
                  ) : null}
                </>
              )}
              <Map
                style={styles.map}
                handleCoordinatesChange={handleCoordinatesChange}
                handleAddressChange={(val) => {
                  handleAddressChange(val);
                }}
              />

              <Text style={styles.textLabel}>Address</Text>
              <TextInput
                name="address"
                label="address relating to the fault"
                mode="outlined"
                onChangeText={(newValue) => {
                  setFieldValue("address", newValue);
                  setPinAddress(newValue);
                  handleChange("address");
                }}
                onBlur={handleBlur("address")}
                value={pinAddress}
                style={styles.textInput}
                multiline={true}
                enableReinitialize={true}
                accessibilityLabel="address relating to the fault"
              />
              {errors.address && touched.address ? (
                <Text style={styles.error}>{errors.address}</Text>
              ) : null}

              <Text style={styles.textLabel}>Your phone number</Text>
              <TextInput
                name="phone"
                label=""
                mode="outlined"
                onChangeText={handleChange("phone")}
                onBlur={handleBlur("phone")}
                value={values.phone}
                style={styles.textInput}
                keyboardType="numeric"
                maxLength={11}
                accessibilityLabel="phone number"
              />
              {errors.phone && touched.phone ? (
                <Text style={styles.error}>{errors.phone}</Text>
              ) : null}
              <Text style={styles.textLabel}>Your email address</Text>
              <TextInput
                name="email"
                label=""
                mode="outlined"
                onChangeText={handleChange("email")}
                onBlur={handleBlur("email")}
                value={values.email}
                style={styles.textInput}
                keyboardType="email-address"
                maxLength={100}
                accessibilityLabel="email address"
              />
              {errors.email && touched.email ? (
                <Text style={styles.error}>{errors.email}</Text>
              ) : null}
            </>
            {parent !== "streetlight" && (
              <>
                <Text style={styles.textLabel}>
                  Are you aware of any access related issues?
                </Text>
                <View style={styles.checkBoxWrapper}>
                  <RadioButton.Group
                    onValueChange={(newValue) => {
                      setFieldValue("accessIssue", newValue);
                      setValue(newValue);
                    }}
                    value={values.accessIssue}
                  >
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "space-around",
                        alignItems: "center",
                        margin: Style.GAP,
                      }}
                    >
                      <Text>Yes</Text>
                      <RadioButton.Android
                        value="Yes"
                        color={Style.COLOR_PRIMARY}
                      />
                      <Text>No</Text>
                      <RadioButton.Android
                        value="No"
                        color={Style.COLOR_PRIMARY}
                      />
                    </View>
                  </RadioButton.Group>
                  {errors.accessIssue && touched.accessIssue ? (
                    <Text style={styles.error}>{errors.accessIssue}</Text>
                  ) : null}
                </View>
              </>
            )}
            <Text style={styles.textLabel}>Additional comments</Text>
            <TextInput
              name="comment"
              label="locked gates / pets etc."
              mode="outlined"
              onChangeText={handleChange("comment")}
              onBlur={handleBlur("comment")}
              value={values.comment}
              style={styles.textInput}
              multiline={true}
              accessibilityLabel="additional comments"
            />
            <PhotoPicker
              numberOfImages={2}
              onChange={(val) => setSelectedImage(val)}
            />
            <ConsentBox
              name="acknowledgement"
              onPress={(val) => {
                setFieldValue("acknowledgement", val);
              }}
              value={values.acknowledgement}
            />
            {errors.acknowledgement && touched.acknowledgement ? (
              <Text style={styles.error}>{errors.acknowledgement}</Text>
            ) : null}

            <TouchableOpacity
              onPress={handleSubmit}
              style={styles.footerContainer}
            >
              <Text
                style={{
                  color: "#ffffff",
                  fontSize: 20,
                }}
              >
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </Formik>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerStyle: {
    backgroundColor: "white",
    padding: 20,
    margin: Style.GAP,
  },
  textLabel: {
    marginTop: Style.GAP,
    marginLeft: Style.GAP,
    fontSize: Style.FONT_SIZE_CARD,
  },
  textInput: {
    margin: Style.GAP,
    marginTop: 0,
    fontSize: Style.FONT_SIZE_CARD,
  },
  photoButtonContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#ffffff",
    padding: Style.GAP * 0.1,
    paddingLeft: 0,
    color: "#fff",
    fontSize: 20,
    borderRadius: 2,
    margin: Style.GAP,
    marginTop: 0,
    borderColor: "#333",
    borderWidth: 1,
  },
  footerContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    backgroundColor: "#2A3D6F",
    padding: Style.GAP * 0.6,
    alignItems: "center",
    color: "#fff",
    fontSize: 20,
    borderRadius: 4,
    fontFamily: "AzoSans-Regular",
    margin: Style.GAP,
  },
  customButton: {
    width: "50%",
    justifyContent: "flex-start",
    textAlign: "left",
    backgroundColor: "#ddd",
    height: 40,
  },
  checkBoxWrapper: {
    flex: 1,
    flexWrap: "wrap",
    textAlign: "center",
  },
  checkBoxStyle: {
    borderColor: Style.COLOR_PRIMARY,
    alignSelf: "flex-end",
    height: 30,
  },
  thumbnail: {
    width: 100,
    height: 100,
    resizeMode: "contain",
  },
  error: {
    paddingLeft: Style.GAP,
    marginTop: 0,
    color: "red",
    fontSize: Style.FONT_SIZE_BODY,
  },
  map: {
    height: Dimensions.get("window").height / 4,
    margin: Style.GAP,
  },
});
