const config = './PowerOptions';


const answer = {
    first: '1'
};

class Question {
    constructor({ id, label, options, children, parent, parentId }) {
        this.id = id;
        this.label = label;
        this.options = options;
        this.parent = parent;
        this.parentId = parentId;
        this.children = buildChildren(children, id);
    }

    shouldShow(parentAnswer) {
        return !this.parent || this.parent === parentAnswer;
    }

    questions() {
        const childrenQuestion = this.children.flatMap(children => children.questions());
        return [this, ...childrenQuestion];
    }
}

function buildChildren(children = [], id) {
    return children.map((child) => new Question({ ...child, parentId: id }));
}

const question = new Question(config);
console.log(question.questions().map(q => ({ label: q.id, show: q.shouldShow(answer[q.parentId]) })));