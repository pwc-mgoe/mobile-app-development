import React from 'react';
import {Linking, ScrollView, StyleSheet, Text} from "react-native";
import {Headline} from "react-native-paper";
import Header from "../../Header";
import * as Style from "../../styles/Style";
import ButtonLarge from "../../ButtonLarge";
import {useNavigation} from "@react-navigation/native";


const StreetlightPreInfo = () => {

  const navigation = useNavigation();

  const handleContinue = () => {
    navigation.navigate('StreetlightOptionsForm');
  }

  return (
    <>
      <Header title={"Report streetlight fault"}/>
      <ScrollView style={{margin: Style.GAP}}>
        <Text style={styles.paragraph}>
          We manage streetlights on major roads owned by the Northern Territory Government as well as in select local
          government jurisdictions such as Alice Springs, Victoria/Daly, Roper Gulf, West Arnhem and Barkly
          regions. {'\n'}{'\n'}
          In these areas, you can report streetlight fault using the form below: To report streetlight faults outside of
          the above areas , please contact the relevant council: {'\n'} {'\n'}
          <Text style={styles.hyperlink} onPress={()=> Linking.openURL('https://www.darwin.nt.gov.au/report_street_light_fault')}>City of Darwin</Text> {'\n'}{'\n'}
          <Text style={styles.hyperlink} onPress={()=> Linking.openURL('https://www.palmerston.nt.gov.au/operations/parking-and-roads/lights') }>City of Palmerston </Text>{'\n'} {'\n'}
          <Text style={styles.hyperlink} onPress={()=> Linking.openURL('https://www.litchfield.nt.gov.au/services-facilities/streetlights')}>Litchfield Shire Council</Text> {'\n'} {'\n'}
          <Text style={styles.hyperlink} onPress={()=> Linking.openURL('https://www.katherine.nt.gov.au/services/roads-and-infrastructure/streetlights.aspx')}>Katherine Town Council</Text> {'\n'} {'\n'}
        </Text>
      </ScrollView>
      <ButtonLarge title={"Continue"} onPress={handleContinue}/>
    </>
  );
};

const styles = StyleSheet.create({
  paragraph: {
    fontFamily: Style.FONT_REGULAR,
    fontSize: Style.FONT_SIZE_CARD
  },
  hyperlink:{
    color:Style.COLOR_PRIMARY
  }
})
export default StreetlightPreInfo;