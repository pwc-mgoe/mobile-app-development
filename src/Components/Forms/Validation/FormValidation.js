//FORM VALIDATION SCHEMA
import * as yup from "yup";
import moment from "moment";

const PowerSinglePhaseValidation = yup.object().shape({
  month: yup
    .string()
    .matches(/^(0[1-9]|1[012])$/, "Invalid Month")
    .test("dobM", "Invalid month", (value) => {
      if (value < 1 || value > 12) {
        return false;
      }
      return true;
    })
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Month is required"),
  year: yup
    .string()
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(4, "Must be 4 digits")
    .max(4, "Must be 4 digits")
    .required("Valid year is required"),
  day: yup
    .string()

    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      // February
      if (month == 2) {
        const isLeapYear =
          year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

        if (day > 29 || (day == 29 && !isLeapYear)) {
          return false;
        }
      }
      return true;
    })
    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      // Check months with less than 31 days
      // 4. April
      // 6. June
      // 9. September
      // 11. November
      if (
        (month == 4 || month == 6 || month == 9 || month == 11) &&
        day == 31
      ) {
        return false;
      }
      return true;
    })
    .test("dobD", "Invalid day", (day) => {
      if (day < 1 || day > 31) {
        return false;
      }
      return true;
    })
    .test("dobD", "Date cannot be in future", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      const today = moment();
      const enteredDate = new Date(year, month - 1, day);

      if (moment(enteredDate).isAfter(today)) {
        return false;
      }
      return true;
    })
    .matches(/^[0-9]+$/, "Digits Only")
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Day is required"),
  meter: yup.string(),
  meternumber: yup.number().required("Meter number is required"),
  meterreading: yup.number().required("Reading is required"),
  phone: yup.number().required("Enter a valid phone number"),

  email: yup
    .string()
    .email("please enter valid email")
    .required("Email address is required"),
  acknowledgement: yup
    .bool()
    .oneOf([true], "Please accept the Privacy Policy")
    .required("Please accept the Privacy Policy"),
});

const PowerThreePhaseValidation = yup.object().shape({
  month: yup
    .string()
    .matches(/^(0[1-9]|1[012])$/, "Invalid Month")
    .test("dobM", "Invalid month", (value) => {
      if (value < 1 || value > 12) {
        return false;
      }
      return true;
    })
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Month is required"),
  year: yup
    .string()
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(4, "Must be 4 digits")
    .max(4, "Must be 4 digits")
    .required("Valid year is required"),
  day: yup
    .string()

    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      // February
      if (month == 2) {
        const isLeapYear =
          year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

        if (day > 29 || (day == 29 && !isLeapYear)) {
          return false;
        }
      }
      return true;
    })
    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      // Check months with less than 31 days
      // 4. April
      // 6. June
      // 9. September
      // 11. November
      if (
        (month == 4 || month == 6 || month == 9 || month == 11) &&
        day == 31
      ) {
        return false;
      }
      return true;
    })
    .test("dobD", "Invalid day", (day) => {
      if (day < 1 || day > 31) {
        return false;
      }
      return true;
    })
    .test("dobD", "Date cannot be in future", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      const today = moment();
      const enteredDate = new Date(year, month - 1, day);

      if (moment(enteredDate).isAfter(today)) {
        return false;
      }
      return true;
    })
    .matches(/^[0-9]+$/, "Digits Only")
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Day is required"),
  meter: yup.string(),
  metertype: yup.string().required(),
  meternumber: yup.number().required("Meter number is required"),
  meterreading: yup.number().required("Reading is required"),
  meternumber2: yup.string().required("Meter number 2 is required"),
  meterreading2: yup.string().required("Reading is required"),
  meternumber3: yup.string().required("Meter number 3 is required"),
  meterreading3: yup.string().required("Reading is required"),
  phone: yup.number().required("Enter a valid phone number"),

  email: yup
    .string()
    .email("please enter valid email")
    .required("Email address is required"),
  acknowledgement: yup
    .bool()
    .oneOf([true], "Please accept the Privacy Policy")
    .required("Please accept the Privacy Policy"),
});

const ElectronicValidation = yup.object().shape({
  month: yup
    .string()
    .matches(/^(0[1-9]|1[012])$/, "Invalid Month")
    .test("dobM", "Invalid month", (value) => {
      if (value < 1 || value > 12) {
        return false;
      }
      return true;
    })
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Month is required"),
  year: yup
    .string()
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(4, "Must be 4 digits")
    .max(4, "Must be 4 digits")
    .required("Valid year is required"),
  day: yup
    .string()

    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      // February
      if (month == 2) {
        const isLeapYear =
          year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

        if (day > 29 || (day == 29 && !isLeapYear)) {
          return false;
        }
      }
      return true;
    })
    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      // Check months with less than 31 days
      // 4. April
      // 6. June
      // 9. September
      // 11. November
      if (
        (month == 4 || month == 6 || month == 9 || month == 11) &&
        day == 31
      ) {
        return false;
      }
      return true;
    })
    .test("dobD", "Invalid day", (day) => {
      if (day < 1 || day > 31) {
        return false;
      }
      return true;
    })
    .test("dobD", "Date cannot be in future", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      const today = moment();
      const enteredDate = new Date(year, month - 1, day);

      if (moment(enteredDate).isAfter(today)) {
        return false;
      }
      return true;
    })
    .matches(/^[0-9]+$/, "Digits Only")
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Day is required"),
  meter: yup.string(),
  metertype: yup.string().required(),
  meternumber: yup.number().required("Meter number is required"),
  meterreading: yup.number().required("Reading is required"),
  meterreading10: yup.number().required("Reading is required"),
  phone: yup.number().required("Enter a valid phone number"),
  email: yup
    .string()
    .email("please enter valid email")
    .required("Email address is required"),
  acknowledgement: yup
    .bool()
    .oneOf([true], "Please accept the Privacy Policy")
    .required("Please accept the Privacy Policy"),
});
const WaterValidation = yup.object().shape({
  month: yup
    .string()
    .matches(/^(0[1-9]|1[012])$/, "Invalid Month")
    .test("dobM", "Invalid month", (value) => {
      if (value < 1 || value > 12) {
        return false;
      }
      return true;
    })
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Month is required"),
  year: yup
    .string()
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(4, "Must be 4 digits")
    .max(4, "Must be 4 digits")
    .required("Valid year is required"),
  day: yup
    .string()

    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      // February
      if (month == 2) {
        const isLeapYear =
          year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

        if (day > 29 || (day == 29 && !isLeapYear)) {
          return false;
        }
      }
      return true;
    })
    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      // Check months with less than 31 days
      // 4. April
      // 6. June
      // 9. September
      // 11. November
      if (
        (month == 4 || month == 6 || month == 9 || month == 11) &&
        day == 31
      ) {
        return false;
      }
      return true;
    })
    .test("dobD", "Invalid day", (day) => {
      if (day < 1 || day > 31) {
        return false;
      }
      return true;
    })
    .test("dobD", "Date cannot be in future", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      const today = moment();
      const enteredDate = new Date(year, month - 1, day);

      if (moment(enteredDate).isAfter(today)) {
        return false;
      }
      return true;
    })
    .matches(/^[0-9]+$/, "Digits Only")
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Day is required"),
  meter: yup.string(),
  meternumber: yup.string().required("Meter number is required"),
  meterreading: yup.number().required("Reading is required"),
  phone: yup.number().required("Enter a valid phone number"),

  email: yup
    .string()
    .email("please enter valid email")
    .required("Email address is required"),
  acknowledgement: yup
    .bool()
    .oneOf([true], "Please accept the Privacy Policy")
    .required("Please accept the Privacy Policy"),
});

const SolarSinglePhaseValidation = yup.object().shape({
  month: yup
    .string()
    .matches(/^(0[1-9]|1[012])$/, "Invalid Month")
    .test("dobM", "Invalid month", (value) => {
      if (value < 1 || value > 12) {
        return false;
      }
      return true;
    })
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Month is required"),
  year: yup
    .string()
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(4, "Must be 4 digits")
    .max(4, "Must be 4 digits")
    .required("Valid year is required"),
  day: yup
    .string()

    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      // February
      if (month == 2) {
        const isLeapYear =
          year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

        if (day > 29 || (day == 29 && !isLeapYear)) {
          return false;
        }
      }
      return true;
    })
    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      // Check months with less than 31 days
      // 4. April
      // 6. June
      // 9. September
      // 11. November
      if (
        (month == 4 || month == 6 || month == 9 || month == 11) &&
        day == 31
      ) {
        return false;
      }
      return true;
    })
    .test("dobD", "Invalid day", (day) => {
      if (day < 1 || day > 31) {
        return false;
      }
      return true;
    })
    .test("dobD", "Date cannot be in future", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      const today = moment();
      const enteredDate = new Date(year, month - 1, day);

      if (moment(enteredDate).isAfter(today)) {
        return false;
      }
      return true;
    })
    .matches(/^[0-9]+$/, "Digits Only")
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Day is required"),

  meter: yup.string(),
  meternumber: yup.number().required("Meter number is required"),
  meterreading003: yup.number().required("003 reading is required"),
  meterreading023: yup.number().required("023 reading is required"),
  phone: yup.number().required("Enter a valid phone number"),

  email: yup
    .string()
    .email("please enter valid email")
    .required("Email address is required"),
  acknowledgement: yup
    .bool()
    .oneOf([true], "Please accept the Privacy Policy")
    .required("Please accept the Privacy Policy"),
});

const SolarThreePhaseValidation = yup.object().shape({
  month: yup
    .string()
    .matches(/^(0[1-9]|1[012])$/, "Invalid Month")
    .test("dobM", "Invalid month", (value) => {
      if (value < 1 || value > 12) {
        return false;
      }
      return true;
    })
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Month is required"),
  year: yup
    .string()
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(4, "Must be 4 digits")
    .max(4, "Must be 4 digits")
    .required("Valid year is required"),
  day: yup
    .string()

    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      // February
      if (month == 2) {
        const isLeapYear =
          year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

        if (day > 29 || (day == 29 && !isLeapYear)) {
          return false;
        }
      }
      return true;
    })
    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      // Check months with less than 31 days
      // 4. April
      // 6. June
      // 9. September
      // 11. November
      if (
        (month == 4 || month == 6 || month == 9 || month == 11) &&
        day == 31
      ) {
        return false;
      }
      return true;
    })
    .test("dobD", "Invalid day", (day) => {
      if (day < 1 || day > 31) {
        return false;
      }
      return true;
    })
    .test("dobD", "Date cannot be in future", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      const today = moment();
      const enteredDate = new Date(year, month - 1, day);

      if (moment(enteredDate).isAfter(today)) {
        return false;
      }
      return true;
    })
    .matches(/^[0-9]+$/, "Digits Only")
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Day is required"),
  meter: yup.string(),
  meternumber: yup.number().required("Meter number is required"),
  meterreading003: yup.number().required("003 reading is required"),
  meterreading023: yup.number().required("Meter number is required"),
  meternumber2: yup.number().required("Meter number is required"),
  meterreading0032: yup.number().required("003 reading is required"),
  meterreading0232: yup.number().required("Meter number is required"),
  meternumber3: yup.number().required("Meter number is required"),
  meterreading0033: yup.number().required("003 reading is required"),
  meterreading0233: yup.number().required("Meter number is required"),
  phone: yup.number().required("Enter a valid phone number"),

  email: yup
    .string()
    .email("please enter valid email")
    .required("Email address is required"),
  acknowledgement: yup
    .bool()
    .oneOf([true], "Please accept the Privacy Policy")
    .required("Please accept the Privacy Policy"),
});

const SolarCombinationValidation = yup.object().shape({
  month: yup
    .string()
    .matches(/^(0[1-9]|1[012])$/, "Invalid Month")
    .test("dobM", "Invalid month", (value) => {
      if (value < 1 || value > 12) {
        return false;
      }
      return true;
    })
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Month is required"),
  year: yup
    .string()
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(4, "Must be 4 digits")
    .max(4, "Must be 4 digits")
    .required("Valid year is required"),
  day: yup
    .string()

    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      // February
      if (month == 2) {
        const isLeapYear =
          year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

        if (day > 29 || (day == 29 && !isLeapYear)) {
          return false;
        }
      }
      return true;
    })
    .test("dobD", "Invalid date", function (day) {
      const month = this.options.parent.month;
      // Check months with less than 31 days
      // 4. April
      // 6. June
      // 9. September
      // 11. November
      if (
        (month == 4 || month == 6 || month == 9 || month == 11) &&
        day == 31
      ) {
        return false;
      }
      return true;
    })
    .test("dobD", "Invalid day", (day) => {
      if (day < 1 || day > 31) {
        return false;
      }
      return true;
    })
    .test("dobD", "Date cannot be in future", function (day) {
      const month = this.options.parent.month;
      const year = this.options.parent.year;
      const today = moment();
      const enteredDate = new Date(year, month - 1, day);

      if (moment(enteredDate).isAfter(today)) {
        return false;
      }
      return true;
    })
    .matches(/^[0-9]+$/, "Digits Only")
    .min(2, "Must be 2 digits")
    .max(2, "Must be 2 digits")
    .required("Day is required"),
  meternumber: yup.number().required("Meter number is required"),
  meterreading04: yup.number().required("04 reading is required"),
  meterreading010: yup.number().required("010 reading is required"),
  meterreading023: yup.number().required("023 reading is required"),
  phone: yup.number().required("Enter a valid phone number"),

  email: yup
    .string()
    .email("please enter valid email")
    .required("Email address is required"),
  acknowledgement: yup
    .bool()
    .oneOf([true], "Please accept the Privacy Policy")
    .required("Please accept the Privacy Policy"),
});

//FORM VALIDATION SCHEMA
const ReportFaultValidation = yup.object().shape({
  phone: yup.number().required("Phone number is required"),
  email: yup
    .string()
    .email("please enter valid email")
    .required("Email address is required"),
  address: yup.string().required("Address is required"),
  accessIssue: yup.string().when("streetlight", {
    is: false,
    then: yup.string().required("Access issue is required."),
  }),
  acknowledgement: yup
    .bool()
    .oneOf([true], "Please accept the Privacy Policy")
    .required("Please accept the Privacy Policy"),
});

export {
  PowerSinglePhaseValidation,
  PowerThreePhaseValidation,
  ElectronicValidation,
  WaterValidation,
  SolarSinglePhaseValidation,
  SolarThreePhaseValidation,
  SolarCombinationValidation,
  ReportFaultValidation,
};
