import React from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { Text } from "react-native-paper";
import * as Style from "./styles/Style";

const hGap = Dimensions.get("window").width * 0.05;

export default function BlockQuote(props) {
  const { content } = props;
  return (
    <View style={styles.quoteContainer}>
      <Text style={styles.quote} numberOfLines={10}>
        {content}
      </Text>
    </View>
  );
}
const styles = StyleSheet.create({
  quoteContainer: {
    backgroundColor: "#EAEAEA",
    borderLeftWidth: 9,
    borderLeftColor: Style.COLOR_PRIMARY,
    padding: hGap,
  },
  quote: {
    color: "#333",
    fontSize: Style.FONT_SIZE_BODY,
    textAlign: "auto",
    fontFamily: "AzoSans-Regular",
    height: "auto",
  },
});
