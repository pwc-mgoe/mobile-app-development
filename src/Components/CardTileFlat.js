import * as React from "react";

import { StyleSheet, View, Dimensions, Pressable, Alert } from "react-native";
import { useTheme, withTheme, Text, TouchableRipple } from "react-native-paper";
import { useNetInfo } from "@react-native-community/netinfo";
import { useNavigation } from "@react-navigation/native";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as Style from "../Components/styles/Style";

function CardTileFlat(props) {
  const { colors, fonts } = useTheme();
  const { icon, title, screen, metertype } = props;
  const networkInfo = useNetInfo();
  const navigation = useNavigation();
  const handlePress = () => {
    if (!networkInfo.isConnected) {
      Alert.alert(
        "Network error",
        " Please check your connection and try again later."
      );
      return;
    }
    navigation.navigate(screen, {
      meter: metertype,
    });
  };

  return (
    <TouchableOpacity key={props.key} onPress={handlePress}>
      <View style={styles.cardItem}>
        <View style={styles.cardIcon}>{icon}</View>
        <Text style={[styles.cardTitle]} color={colors.primary}>
          {title}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardItem: {
    width: Dimensions.get("window").width * 0.8,
    aspectRatio: 3.5,
    flexDirection: "row",
    alignItems: "center",
    margin: Style.GAP * 0.5,
    padding: Style.GAP * 0.5,
    backgroundColor: Style.COLOR_PRIMARY,
  },
  cardIcon: {
    alignItems: "flex-start",
    flex: 2,
    paddingLeft: Style.GAP * 2,
    // backgroundColor: '#ddd'
  },
  cardTitle: {
    flex: 5,
    fontSize: Style.FONT_SIZE_CARD * 1.2,
    // marginTop: Style.GAP,
    textAlign: "left",
    fontFamily: "AzoSans-Bold",
    color: "#fff",
    justifyContent: "center",
  },
});

export default CardTileFlat;
