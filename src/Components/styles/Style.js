
import React from "react";
import { Dimensions } from "react-native";

//Default Gap
export const GAP = Dimensions.get("window").width * 0.05;


//Default Styles
export const COLOR_PRIMARY = '#2A3D6F';
export const COLOR_WHITE = '#ffffff';
export const COLOR_SECONDARY = '#75842f';
export const FONT_REGULAR = 'AzoSans-Regular';
export const FONT_BOLD = 'AzoSans-Bold';
export const BORDER_RADIUS = 5;
export const FONT_SIZE_BODY = 14;
export const FONT_SIZE_CARD = 18;
export const FONT_SIZE_HEADING = 24;