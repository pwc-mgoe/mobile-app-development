import React from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import { List, TouchableRipple } from "react-native-paper";
import { SafeAreaView } from "react-native-safe-area-context";
import moment from "moment";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as Linking from "expo-linking";


const hGap = Dimensions.get("window").width * 0.05;

export default function ListItem(props) {
  const { created_at, text, entities } = props.data;
  const dateField = new Date(created_at);

  const handlePress = () => {
    console.log(entities.urls)
    entities.urls[0] ? Linking.openURL(entities.urls[0].url) : '';
  };

  React.useEffect(() => {});

  if (!created_at) {
    return (
      <>
        <SafeAreaView>
          <Text style={{textAlign:'center'}}>Loading</Text>
        </SafeAreaView>
      </>
    );
  }
  return (
    <>
      <TouchableOpacity
        style={{
          flex: 1,
          margin: hGap,
          borderBottomColor: "#2A3D6F",
          borderBottomWidth: 1,
          padding: 0
        }}
        onPress={handlePress}
      >
        <View>
          <Text
            style={{
              lineHeight: 24,
              color: "#2A3D6F",
              fontFamily:'AzoSans-Bold'
            }}
          >
            {moment(dateField).format("ddd, ll")}
          </Text>
          <Text
            style={{
              lineHeight: 24,
              color: "rgb(117,132,47)",
              fontFamily:'AzoSans-Regular'
            }}
          >
            @{moment(dateField).format("LT")}
          </Text>
          <Text style={{fontFamily:'AzoSans-Regular'}} >{text}</Text>
          <Text></Text>
        </View>
      </TouchableOpacity>
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    fontFamily: 'AzoSans-Regular',
    borderBottomColor: "#2A3D6F",
    borderBottomWidth: 1,
    margin: hGap * 0.5,
    color: "#2A3D6F"
  }
});
