import * as React from "react";

import { StyleSheet, View, Dimensions, Pressable, Alert } from "react-native";
import { useTheme, withTheme, Text, TouchableRipple } from "react-native-paper";
import { useNetInfo } from "@react-native-community/netinfo";
import { useNavigation } from "@react-navigation/native";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as Style from "../Components/styles/Style";
const windowWidth = Dimensions.get("window").width;

const hGap = windowWidth * 0.05;

function CardTileReversed(props) {
  const { colors, fonts } = useTheme();
  const { icon, title, screen, metertype } = props;
  const networkInfo = useNetInfo();
  const navigation = useNavigation();
  const handlePress = () => {
    if (!networkInfo.isConnected) {
      Alert.alert(
        "Network error",
        " Please check your connection and try again later."
      );
      return;
    }
    navigation.navigate(screen, {
      meter: metertype,
    });
  };

  return (
    <TouchableOpacity key={props.key} onPress={handlePress}>
      <View style={styles.cardItem}>
        {icon}
        <Text style={[styles.cardTitle]} color={colors.primary}>
          {title}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardItem: {
    width: windowWidth * 0.4,
    aspectRatio: 1 / 1,
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    margin: hGap * 0.6,
    padding: 8,
    borderRadius: 4,
    fontFamily: "AzoSans-Regular",
    backgroundColor: Style.COLOR_PRIMARY,
  },
  cardTitle: {
    fontSize: Style.FONT_SIZE_CARD,
    marginTop: hGap,
    alignContent: "center",
    textAlign: "center",
    fontFamily: "AzoSans-Bold",
    color: "#fff",
  },
});

export default CardTileReversed;
