import * as React from "react";
import { StyleSheet, View, Dimensions } from "react-native";
import { Text, useTheme } from "react-native-paper";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import * as Linking from "expo-linking";
import * as Style from "../Components/styles/Style";

const hGap = Dimensions.get("window").width * 0.05;

function BannerArea(props) {
  const { fonts } = useTheme();
  const { theme } = props;
  const navigation = useNavigation();

  const _handleLinking = (value) => {
    if (value === "about") {
      navigation.navigate("About");
    } else {
      Linking.openURL("https://www.powerwater.com.au/privacy");
    }
  };

  return (
    <View style={{ flex: 1, justifyContent: "center", alignContent: "center" }}>
      <View style={styles.bannerItem}>
        <TouchableOpacity onPress={() => _handleLinking("about")}>
          <Text style={[styles.bannerText, { fontFamily: Style.FONT_BOLD }]}>
            About
          </Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={[styles.bannerText, { fontFamily: Style.FONT_BOLD }]}>
            {Platform.OS === "ios" ? `${" "}|${"   "}` : `${"   "}|${"   "}`}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => _handleLinking("privacy")}>
          <Text style={[styles.bannerText, { fontFamily: Style.FONT_BOLD }]}>
            Privacy
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  bannerItem: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end",
    alignContent: "center",
    margin: hGap,
    alignSelf: "center",
  },
  bannerText: {
    fontFamily: Style.FONT_BOLD,
    fontSize: Style.FONT_SIZE_CARD,
    alignSelf: "center",
    textAlign: "center",
    color: "#fff",
  },
});

export default BannerArea;
