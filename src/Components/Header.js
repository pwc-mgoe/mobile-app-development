import React from "react";
import { View, StyleSheet, Dimensions, Platform } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Text } from "react-native-paper";
import BACK_ICON from "../../assets/icons/back-svgrepo-com.svg";
import { TouchableOpacity } from "react-native-gesture-handler";
import { SafeAreaView } from "react-native-safe-area-context";
import * as Style from "../Components/styles/Style";

export default function Header({ title }) {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity
        onPress={navigation.goBack}
        style={{
          flex: 1,
          justifyContent: "center",
          marginLeft: Style.GAP,
          paddingRight: Style.GAP * 0.5,
        }}
      >
        <BACK_ICON width={36} height={36} fill={"#ffffff"} />
      </TouchableOpacity>
      <View
        style={{
          flex: 4,
        }}
      >
        <Text
          style={{
            justifyContent: "center",
            alignSelf: "center",
            fontSize: 26,
            color: "#fff",
            fontFamily: "AzoSans-Bold",
          }}
          adjustsFontSizeToFit
        >
          {title}
        </Text>
      </View>
      <View
        style={{
          flex: 1,
          width: "100%",
          justifyContent: "center",
        }}
      ></View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: Style.GAP,
    paddingBottom: Style.GAP,
    // height: Platform.OS === "android" ? Style.GAP * 6 : "auto",
    flexWrap: "wrap",
    flexDirection: "row",
    backgroundColor: "#2A3D6F",
    color: "#ffffff",
  },
});
