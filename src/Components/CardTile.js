import * as React from "react";

import { StyleSheet, View, Dimensions, Pressable, Alert } from "react-native";
import { useTheme, withTheme, Text } from "react-native-paper";
import { useNetInfo } from "@react-native-community/netinfo";
import { useNavigation } from "@react-navigation/native";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as Style from "./styles/Style";

const windowWidth = Dimensions.get("window").width;

const hGap = windowWidth * 0.05;

function CardTile(props) {
  const { colors, fonts } = useTheme();
  const { icon, title, theme, screen, bg } = props;
  const networkInfo = useNetInfo();
  const navigation = useNavigation();
  const handlePress = () => {
    if (!networkInfo.isConnected) {
      Alert.alert(
        "Network error",
        " Please check your connection and try again later."
      );
      return;
    }
    navigation.navigate(screen);
  };

  return (
    <TouchableOpacity key={props.key} onPress={handlePress}>
      <View style={styles.cardItem}>
        {icon}
        <Text
          style={[
            styles.cardTitle,
            { color: colors.primary, fontFamily: Style.FONT_BOLD },
          ]}
          color={colors.primary}
        >
          {title}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardItem: {
    width: windowWidth * 0.4,
    aspectRatio: 1 / 1,
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    margin: hGap * 0.6,
    padding: 8,
    borderRadius: 4,
    backgroundColor: "#EAEAEA",
  },
  cardTitle: {
    fontSize: 16,
    marginTop: hGap,
    alignContent: "center",
    textAlign: "center",
  },
});

export default CardTile;
