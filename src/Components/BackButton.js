import React from "react";
import { View, Text } from "react-native";

import BACK_ICON from "../../assets/icons/back-svgrepo-com.svg";
import { useNavigation } from "@react-navigation/native";
import { TouchableOpacity } from "react-native-gesture-handler";

export default function BackButton(props) {
  const { navigation } = useNavigation();
  return (
    <>
      <TouchableOpacity>
        <BACK_ICON
          width={30}
          height={30}
          fill={"#fff"}
          style={{
            padding: 5,
          }}
        />
      </TouchableOpacity>
    </>
  );
}
