import React from 'react';
import {TouchableOpacity} from "react-native-gesture-handler";
import REPORT_ICON from "../../assets/icons/report-problem-EPS_crop.svg";
import {Dimensions, StyleSheet } from "react-native";
import {Text} from 'react-native-paper';

const hGap = Dimensions.get("window").width * 0.05;

function ButtonLarge(props) {
  return (
    <>
      <TouchableOpacity style={styles.footerContainer} onPress={props.onPress}>
        <REPORT_ICON width={36} height={36} fill={"#ddd"} />
        <Text
          style={{
            color: "#fff",
            fontSize: 20,
            marginLeft: hGap
          }}
        >
          {props.title}
        </Text>
      </TouchableOpacity>
    </>
  );
}
const styles = StyleSheet.create({
  footerContainer: {
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    margin: hGap * 0.5,
    backgroundColor: "#2A3D6F",
    padding: hGap * 0.6,
    marginBottom: hGap,
    alignItems: "center",
    color: "#ffffff",
    fontSize: 20,
    borderRadius: 4,
    fontFamily:'AzoSans-Regular'
  }
})
export default ButtonLarge;