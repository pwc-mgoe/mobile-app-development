import React, { useEffect, useState } from "react";
import { View, StyleSheet, Alert, Image, ImageBackground } from "react-native";
import {
  Text,
  Button,
  Modal,
  Portal,
  IconButton,
  Colors,
} from "react-native-paper";
import * as Style from "./styles/Style";
import * as ImagePicker from "expo-image-picker";

function PhotoPicker(props) {
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedImages, setSelectedImages] = useState([]);

  // OPEN MODAL FOR PHOTO
  const showModal = () => setModalVisible(true);
  const hideModal = () => setModalVisible(false);

  // Capture image from camera
  let openCameraAsync = async () => {
    try {
      let { status } = await ImagePicker.requestCameraPermissionsAsync();
      if (!(status === "granted")) {
        Alert.alert(
          "Sorry, we need camera roll permissions to make this work!"
        );
        return;
      }
      let cameraResult = await ImagePicker.launchCameraAsync({
        base64: true,
        aspect: [4, 3],
        quality: 0.3,
      });
      if (cameraResult.cancelled === true) {
        return;
      }
      setModalVisible(false);

      let imageArray = {
        localUri: cameraResult?.uri,
        base64Uri: cameraResult?.base64,
      };

      await setSelectedImages(
        [imageArray, ...selectedImages].slice(0, props.numberOfImages)
      );
    } catch (e) {
      console.warn(e);
    }
  };

  //Image picker from gallery
  let openImagePickerAsync = async () => {
    try {
      let { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
      console.log(status);
      if (!(status === "granted")) {
        Alert.alert("You need to allow the camera permissions");
        return;
      }
      let libraryResult = await ImagePicker.launchImageLibraryAsync({
        base64: true,
        aspect: [4, 3],
        quality: 0.3,
      });
      if (libraryResult.cancelled === true) {
        return;
      }
      setModalVisible(false);

      let imageArray = {
        localUri: libraryResult?.uri,
        base64Uri: libraryResult?.base64,
      };
      await setSelectedImages(
        [imageArray, ...selectedImages].slice(0, props.numberOfImages)
      );
    } catch (e) {
      console.warn(e);
    }
  };

  //remove array from state
  let onCancelPress = async (image) => {
    const newState = selectedImages.filter(
      (data) => data.localUri !== image.localUri
    );
    console.log(typeof newState);
    setSelectedImages(newState);
    // props.onChange(selectedImages);
  };

  useEffect(() => {
    props.onChange(selectedImages);
  }, [selectedImages]);

  return (
    <>
      {modalVisible && (
        <View>
          <Portal>
            <Modal
              visible={modalVisible}
              onDismiss={hideModal}
              contentContainerStyle={styles.containerStyle}
            >
              <Button
                mode="outlined"
                style={{
                  position: "relative",
                  alignItems: "center",
                  alignSelf: "flex-end",
                }}
                icon="close"
                onPress={() => hideModal()}
              />
              <Button
                mode="contained"
                style={styles.footerContainer}
                icon="camera"
                onPress={openCameraAsync}
                uppercase={false}
              >
                From camera
              </Button>
              <Button
                mode="contained"
                style={styles.footerContainer}
                icon="upload"
                onPress={openImagePickerAsync}
                uppercase={false}
              >
                Upload from gallery
              </Button>
            </Modal>
          </Portal>
        </View>
      )}
      <Text style={styles.textLabel}> Upload or take a photo</Text>
      <View style={styles.photoButtonContainer}>
        <Button
          color="#333333"
          mode="default"
          uppercase={false}
          onPress={showModal}
          style={styles.customButton}
        >
          Select
        </Button>
      </View>
      <View style={styles.imageContainer}>
        {selectedImages &&
          selectedImages.map((image) => (
            <ImageBackground
              source={{ uri: image.localUri }}
              style={styles.thumbnail}
              key={image.localUri}
            >
              <IconButton
                icon="close"
                color={Colors.blueGrey700}
                size={16}
                onPress={() => onCancelPress(image)}
                style={{
                  backgroundColor: "#ffffff80",
                  position: "absolute",
                  right: 0,
                }}
              />
            </ImageBackground>
          ))}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  imageContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  thumbnail: {
    marginLeft: Style.GAP,
    margin: Style.GAP * 0.1,
    width: 100,
    height: 100,
    resizeMode: "contain",
  },
  containerStyle: {
    backgroundColor: "white",
    padding: 20,
    margin: Style.GAP,
  },
  photoButtonContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#ffffff",
    padding: Style.GAP * 0.1,
    paddingLeft: 0,
    color: "#fff",
    fontSize: 20,
    borderRadius: 2,
    margin: Style.GAP,
    marginTop: 0,
    borderColor: "#333",
    borderWidth: 1,
  },
  textLabel: {
    marginTop: Style.GAP,
    marginLeft: Style.GAP,
    fontSize: Style.FONT_SIZE_CARD,
  },
  footerContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    backgroundColor: "#2A3D6F",
    padding: Style.GAP * 0.6,
    alignItems: "center",
    color: "#fff",
    fontSize: 20,
    borderRadius: 4,
    fontFamily: "AzoSans-Regular",
    margin: Style.GAP,
  },
  customButton: {
    width: "50%",
    justifyContent: "flex-start",
    textAlign: "left",
    backgroundColor: "#ddd",
    height: 40,
  },
});

export default PhotoPicker;
