import React from "react";
import { View, Text, Image } from "react-native";
import Header from "../Components/Header";
import { SafeAreaView } from "react-native-safe-area-context";
import { Headline } from "react-native-paper";
import * as Style from "../Components/styles/Style";
import { ImageBackground } from "react-native-web";

export default function About() {
  return (
    <View style={{ flex: 1 }}>
      <Header title="About" />
      <View
        style={{
          flex: 1,
          margin: Style.GAP,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {/*        <Image
          resizeMode="center"
          alignSelf="center"
          source={require("../../assets/img/logo.png")}
        />*/}
        <Headline
          style={{
            margin: Style.GAP,
            textAlign: "center",
            fontFamily: Style.FONT_BOLD,
          }}
        >
          Power and Water mobile app
        </Headline>
        <Text style={{ margin: Style.GAP, fontSize: Style.FONT_SIZE_CARD }}>
          Version: 3.0.0
        </Text>
        <Text style={{ margin: Style.GAP, fontSize: Style.FONT_SIZE_CARD }}>
          {" "}
          Power and Water Corporation 2021{" "}
        </Text>
      </View>
    </View>
  );
}
