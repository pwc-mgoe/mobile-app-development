import * as React from "react";
import { View, StyleSheet, Image, Dimensions } from "react-native";
import { Text, useTheme } from "react-native-paper";
import CardTile from "../Components/CardTile";
import { ScrollView } from "react-native-gesture-handler";
import BannerArea from "../Components/BannerArea";

// Load SVG Icons
import OUTAGE_ICON from "../../assets/icons/outage-exclamation-EPS_crop.svg";
import REPORT_ICON from "../../assets/icons/report-problem-EPS_crop.svg";
import METER_ICON from "../../assets/icons/meter-reads-EPS_crop.svg";
import PAYBILLS_ICON from "../../assets/icons/pay-bills-EPS_crop.svg";
import DAM_ICON from "../../assets/icons/dam-levels-EPS_crop.svg";
import CONTACT_ICON from "../../assets/icons/contact-us-EPS_crop.svg";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

const logoSize = 48;
const hGap = windowWidth * 0.03;

export default function HomeScreen(props) {
  const { colors } = useTheme();

  //render the component
  return (
    <>
      <ScrollView style={styles.container}>
        <View style={styles.column}>
          <View style={styles.logoContainer}>
            <Image
              style={styles.logoItem}
              resizeMode="contain"
              source={require("../../assets/img/logo-reverse.png")}
            />
          </View>
        </View>
        <View style={styles.cardContainer}>
          <View style={styles.column}>
            <CardTile
              title="Outages"
              screen="Outages"
              icon={
                <OUTAGE_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={colors.primary}
                />
              }
            />
            <CardTile
              title="Make a payment"
              screen="Payment"
              icon={
                <PAYBILLS_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={colors.primary}
                />
              }
            />

            <CardTile
              title="Report a problem"
              screen="ReportProblem"
              icon={
                <REPORT_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={colors.primary}
                />
              }
            />
          </View>
          <View style={styles.column}>
            <CardTile
              title="Submit meter reading"
              screen="SubmitReading"
              icon={
                <METER_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={colors.primary}
                />
              }
            />
            <CardTile
              title="Check dam levels"
              screen="DamLevels"
              icon={
                <DAM_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={colors.primary}
                />
              }
            />
            <CardTile
              title="Get in touch"
              screen="GetInTouch"
              icon={
                <CONTACT_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={colors.primary}
                />
              }
            />
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <BannerArea />
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    top: 0,
    height: windowHeight,
    width: windowWidth,
    flexDirection: "column",
    backgroundColor: "#2a3d6f",
    padding: hGap,
  },
  logoContainer: {
    flex: 1,
    paddingTop: hGap * 3,
    flexDirection: "column",
  },
  logoItem: {
    alignSelf: "center",
    width: windowWidth * 0.5,
  },
  cardContainer: {
    flex: 3,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "space-around",
  },
  column: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    marginRight: 0,
    marginLeft: 0,
  },
  bottomContainer: {
    flex: 1,
    flexDirection: "column",
    alignContent: "center",
    justifyContent: "center",
    height: windowWidth * 0.2,
  },
});
