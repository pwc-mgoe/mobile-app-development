import React from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import axios from "axios";
// import qs from "qs";
import Header from "../Components/Header";
import BlockQuote from "../Components/BlockQuote";
import { TextInput } from "react-native-paper";
import { CreditCardInput } from "react-native-credit-card-input";

const hGap = Dimensions.get("window").width * 0.05;

const apiKey = "&API=d43e4fc2-604e-4f56-a567-75c7d71dde72";
const test_mode = true; //Set to false to use Production Credentials and NAB services.  Otherwise TESTING values will be used.
const console_messages = true; //Set to false to disable all console messages.

const FINGERPRINT_PAYMENT_REQUEST_URI =
  "http://webservices.powerwater.com.au/b_auth_sec_test.php";
const FINGERPRINT_TYPE = "payment";
const FINGERPRINT_AMOUNT = 10;
const FINGERPRINT_REQUEST_API_KEY = "d43e4fc2-604e-4f56-a567-75c7d71dde72";
const ReferenceNumber = 234234123412341234;

const headers = { "Content-Type": "application/json" };
const nabHeader = {
  "Content-Type": "application/x-www-form-urlencoded",
};

const requestURI =
  FINGERPRINT_PAYMENT_REQUEST_URI +
  "?TYPE=" +
  FINGERPRINT_TYPE +
  "&REFERENCE=" +
  ReferenceNumber +
  "&AMOUNT=" +
  FINGERPRINT_AMOUNT +
  "&API=" +
  FINGERPRINT_REQUEST_API_KEY;

const config = {
  method: "GET",
  headers: {
    ...headers,
  },
  url: requestURI,
};

async function makeFingerprintRequest() {
  const { data } = await axios(config);

  const payload = {
    EPS_MERCHANT: data.Merchant,
    EPS_TXNTYPE: 0,
    EPS_REFERENCEID: ReferenceNumber,
    EPS_AMOUNT: FINGERPRINT_AMOUNT,
    EPS_TIMESTAMP: data.TimeStamp,
    EPS_FINGERPRINT: data.FingerPrint,
    EPS_RESULTURL: data.Redirect,
    EPS_CARDNUMBER: "3333333333333332",
    EPS_EXPIRYMONTH: 6,
    EPS_EXPIRYYEAR: 24,
    EPS_CCV: "333",
  };

  const nabConfig = {
    method: "POST",
    headers: {
      ...nabHeader,
    },
    data: qs.stringify(payload),
    url: "https://demo.transact.nab.com.au/live/directpostv2/authorise",
  };

  const tx = await axios(nabConfig);
  console.error(tx);
}

export default function MakePayment() {
  makeFingerprintRequest();

  return (
    <>
      <Header title="Make payment" />
      <View>
        <View>
          <BlockQuote content="You will need the 17 digit reference number from your bill" />
          <CreditCardInput allowScroll={true} scale={1} />
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    marginTop: hGap * 0.5,
    color: "#2A3D6F",
    backgroundColor: "#fff",
  },
  heading: {
    flex: 1,
    fontSize: 20,
    fontWeight: "bold",
    alignItems: "center",
    textAlign: "center",
    marginTop: hGap,
    color: "#2A3D6F",
  },
  footerContainer: {
    flex: 1,
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    margin: hGap * 0.5,
    backgroundColor: "#2A3D6F",
    padding: hGap * 0.6,
    marginBottom: hGap,
    alignItems: "center",
    color: "#ffffff",
    fontSize: 20,
    borderRadius: 4,
  },
});
