import * as React from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  Linking,
} from "react-native";
import { Text, Button, ActivityIndicator } from "react-native-paper";
import axios from "axios";
import ListItem from "../Components/ListItem";
import REPORT_ICON from "../../assets/icons/report-problem-EPS_crop.svg";
import { useNavigation } from "@react-navigation/native";
import Header from "../Components/Header";
import { TouchableOpacity } from "react-native-gesture-handler";
import ButtonLarge from "../Components/ButtonLarge";
import * as Style from "../Components/styles/Style";

const hGap = Dimensions.get("window").width * 0.05;

//TODO: Separate the feed function from the component

const count = 30;
const BASE_URL =
  "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=PowerWaterCorp&count=" +
  count;
const TWITTER_BEARER_TOKEN =
  "AAAAAAAAAAAAAAAAAAAAAMHHvAAAAAAAYjVqrmXi0ZCsbudvR5JsiCgp5K0%3DqPfB6MJAyFYZzVaccCtgQm9PWPlXhkxqwosnkhbcANL13OVtMF";
const headers = { "Content-Type": "application/json" };
if (TWITTER_BEARER_TOKEN) {
  headers.Authorization = `Bearer ${TWITTER_BEARER_TOKEN}`;
}

const config = {
  method: "GET",
  headers: {
    ...headers,
  },
  url: BASE_URL,
};

export default function Outages() {
  const [twitterFeed, setTwitterFeed] = React.useState([""]);
  const [isLoading, setIsLoading] = React.useState(true);
  const navigation = useNavigation();

  // get load the twitter data
  async function fetchData() {
    try {
      const { data } = await axios(config);
      setTwitterFeed(data);
    } catch (e) {
      console.error(e);
    }
  }

  React.useEffect(() => {
    fetchData();
    return () => {};
  }, []);

  React.useEffect(() => {
    if (isLoading) {
      setIsLoading(false);
    }
  }, [isLoading]);

  React.useEffect(() => {
    return () => {};
  }, []);

  const handleOutagePress = () => {
    navigation.navigate("ReportProblem");
  };

  if (isLoading) {
    return (
      <>
        <ScrollView>
          <View style={styles.container}>
            <ActivityIndicator />
          </View>
        </ScrollView>
      </>
    );
  }

  return (
    <>
      <Header title="Outages" />
      <ScrollView style={{ flex: 1, backgroundColor: "#ffffff" }}>
        <View style={styles.container}>
          <Button
            uppercase={false}
            style={styles.subHeading}
            mode={"contained"}
            onPress={() =>
              Linking.openURL(
                "https://www.powerwater.com.au/customers/outages/planned-works"
              )
            }
          >
            See all planned works
          </Button>
          <Text style={styles.heading}>
            View current outages and planned power works
          </Text>

          {twitterFeed.map((item, index) => {
            return <ListItem key={index} data={item} />;
          })}
        </View>
      </ScrollView>
      <ButtonLarge title="Report an outage" onPress={handleOutagePress} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    marginTop: hGap * 0.5,
    color: "#2A3D6F",
    backgroundColor: "#fff",
  },
  heading: {
    flex: 1,
    fontSize: 20,
    alignItems: "center",
    textAlign: "center",
    marginTop: hGap,
    color: "#2A3D6F",
  },
  subHeading: {
    flex: 1,
    fontSize: 14,
    alignItems: "center",
    textAlign: "center",
    marginTop: hGap,
    marginLeft: hGap * 4.5,
    marginRight: hGap * 4.5,
    color: "#2A3D6F",
    fontFamily: Style.FONT_BOLD,
    width: "auto",
  },
  footerContainer: {
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    margin: hGap * 0.5,
    backgroundColor: "#2A3D6F",
    padding: hGap * 0.6,
    marginBottom: hGap,
    alignItems: "center",
    color: "#ffffff",
    fontSize: 20,
    borderRadius: 4,
    fontFamily: Style.FONT_BOLD,
  },
});
