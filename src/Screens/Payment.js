import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  Alert,
  Image,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import Header from "../Components/Header";
import BlockQuote from "../Components/BlockQuote";
import * as Style from "../Components/styles/Style";
import {
  Text,
  TextInput,
  ActivityIndicator,
  Portal,
  Modal,
  Headline,
} from "react-native-paper";
import { Formik, validateYupSchema } from "formik";
import * as yup from "yup";
import valid from "card-validator";
import { MakePayment } from "../Utils/PaymentUtils";
import { useNavigation } from "@react-navigation/native";
import { matches } from "underscore";

//PAYMENT FORM VALIDATION SCHEMA
// const amountValidation = /^[0-9]+\.[0-9]{2}$|[0-9]+\.[0-9]{2}[^0-9]/
const formValidationSchema = yup.object().shape({
  crnNumber: yup
    .string()
    .required("CRN is required")
    .test("len", "Must be exactly 17 digits", (val) => {
      if (val) {
        return val.toString().length === 17;
      }
    })
    .typeError("CRN must be a number"),

  cardNumber: yup
    .string()
    .test(
      "test-number",
      "Credit card number is invalid",
      (value) => valid.number(value).isPotentiallyValid
    )
    .required("Credit card number is required")
    .typeError("Credit card must be a number"),

  expiration: yup
    .string()
    .test(
      "test-string",
      "Expiry date is invalid",
      (value) => valid.expirationDate(value).isPotentiallyValid
    )
    .required("Expiry date is required"),

  cvv: yup
    .string()
    .test(
      "test-number",
      "CCV is invalid",
      (value) => valid.cvv(value).isPotentiallyValid
    )
    .required("CCV is required"),

  amount: yup
    .number()
    .required("Amount is required")
    // .matches(amountValidation, 'Please enter 2 digits for the cents value.')
    .min(20.0, "Minimum amount is $20.00")
    .max(5000, "Maximum amount is $5000.00")
    .typeError("Amount must be a number"),
  // .test(
  //   "is-decimal",
  //   "Please enter 2 digits for the cents value.",
  //   // value => (value + "").match(/^\d*\.{1}\d*$/),
  //   (value) => (value + "").match(/^[0-9]*\.(2)[0-9]+$/)
  //   // (value) => {if(value) {
  //   //   console.log(value.toFixed(2));
  //   //   return value.toFixed(2);
  //   // }}
  //   // (value) => Number.isInteger(value * (10 ** 2))
  // ),
});

export default function Payment() {
  const navigation = useNavigation();
  const [dueDate, setDueDate] = useState("");
  const [loading, setLoading] = useState(false);

  const containerStyle = {
    backgroundColor: "#ffffff",
    padding: 20,
    margin: Style.GAP,
  };

  const onInputChange = (setState, value) => {
    if (!isNaN(value)) {
      setState(value);
    }
  };

  useEffect(() => {
    if (dueDate.length === 2) {
      setDueDate(dueDate + "/");
    }
  }, [dueDate]);

  const handlePayment = async (values) => {
    try {
      setLoading(true);
      const result = await MakePayment(values).catch((e) => {
        Alert.alert("Declined", e.toString());
      });
      setLoading(false);
      // console.log(result);
      const errorCategory = ["00", "08", "11"];
      if (errorCategory.includes(result.rescode)) {
        navigation.navigate("ThankYou", {
          msg: "Thank you for your payment. \n\nReceipt number "+ result.txnid,
        });
      }
    } catch (e) {
      // console.log(e);
    }
  };

  return (
    <>
      <Header title="Make a payment" />
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.container}
        keyboardVerticalOffset={Platform.select({
          ios: Style.GAP,
          android: Style.GAP,
        })}
      >
        <ScrollView>
          {loading && (
            <View>
              <Portal>
                <Modal
                  visible={loading}
                  style={{ opacity: 1 }}
                  contentContainerStyle={containerStyle}
                  onDismiss={() => {}}
                >
                  <ActivityIndicator animating={true} size="large" />
                  <Headline style={{ textAlign: "center", margin: Style.GAP }}>
                    Your payment is being processed. please hold...
                  </Headline>
                </Modal>
              </Portal>
            </View>
          )}
          <BlockQuote content="You will need the 17 digit reference number from your bill" />
          <Formik
            validationSchema={formValidationSchema}
            initialValues={{
              crnNumber: "",
              cardNumber: "",
              expiration: dueDate,
              cvv: "",
            }}
            onSubmit={(values) => {
              // console.log(values);
              handlePayment(values);
            }}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              touched,
              errors,
            }) => (
              <View>
                <Text style={styles.textLabel}>Customer reference number</Text>
                <TextInput
                  name="crnNumber"
                  label="17 digit CRN number"
                  mode="outlined"
                  onChangeText={handleChange("crnNumber")}
                  onBlur={handleBlur("crnNumber")}
                  value={values.crnNumber}
                  style={styles.textInput}
                  keyboardType="numeric"
                  maxLength={17}
                  autofocus={true}
                  accessibilityLabel="17 digit CRN number"
                />
                {errors.crnNumber && touched.crnNumber ? (
                  <Text style={styles.error}>{errors.crnNumber}</Text>
                ) : null}
                <Text style={styles.textLabel}>Your card number</Text>
                <View style={{ flexWrap: "wrap", flexDirection: "row" }}>
                  <Image
                    style={{
                      width: 60,
                      height: 60,
                      marginLeft: Style.GAP,
                      marginTop: Style.GAP,
                      marginBottom: 0,
                    }}
                    source={require("../../assets/icons/visa_icon.png")}
                  />
                  <Image
                    style={{
                      width: 80,
                      height: 60,
                      marginTop: Style.GAP,
                      marginBottom: 0,
                    }}
                    source={require("../../assets/icons/mc_icon.png")}
                  />
                </View>
                <TextInput
                  name="cardNumber"
                  label="your card number"
                  mode="outlined"
                  onChangeText={handleChange("cardNumber")}
                  onBlur={handleBlur("cardNumber")}
                  // value={(values.cardNumber.length % 5)===0 ? values.cardNumber+' '  : values.cardNumber}
                  value={values.cardNumber}
                  style={styles.textInput}
                  keyboardType="numeric"
                  maxLength={16}
                  accessibilityLabel="your card number"
                />
                {errors.cardNumber && touched.cardNumber ? (
                  <Text style={styles.error}>{errors.cardNumber}</Text>
                ) : null}
                <View style={{ flexDirection: "row", width: 1000 }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={styles.textLabel}>Expiry</Text>
                    <TextInput
                      name="expiration"
                      label="mm/yy"
                      mode="outlined"
                      onChangeText={handleChange("expiration")}
                      onChange={(values) => onInputChange(setDueDate, values)}
                      onBlur={handleBlur("expiration")}
                      value={
                        values.expiration.length === 2
                          ? values.expiration + "/"
                          : values.expiration
                      }
                      style={styles.textInput}
                      width={100}
                      maxLength={5}
                      keyboardType="numeric"
                      accessibilityLabel="expiry date"
                    />
                    {errors.expiration && touched.expiration ? (
                      <Text style={styles.error}>{errors.expiration}</Text>
                    ) : null}
                  </View>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={styles.textLabel}>CCV</Text>
                    <TextInput
                      name="cvv"
                      label=""
                      mode="outlined"
                      onChangeText={handleChange("cvv")}
                      onBlur={handleBlur("cvv")}
                      value={values.cvv}
                      style={styles.textInput}
                      keyboardType="numeric"
                      width={100}
                      maxLength={3}
                      accessibilityLabel="cvv number"
                    />
                    {errors.cvv && touched.cvv ? (
                      <Text style={styles.error}>{errors.cvv}</Text>
                    ) : null}
                  </View>
                </View>
                <Text style={styles.textLabel}>Amount to pay</Text>
                <TextInput
                  name="amount"
                  label="(ex: 200.00)"
                  mode="outlined"
                  onChangeText={handleChange("amount")}
                  onBlur={handleBlur("amount")}
                  value={values.amount}
                  style={styles.textInput}
                  keyboardType="numeric"
                  accessibilityLabel="amount"
                />
                <Text style={{ marginLeft: Style.GAP }}>
                  * Provide valid amount between $20 and $5000
                </Text>
                {errors.amount && touched.amount ? (
                  <Text style={styles.error}>{errors.amount}</Text>
                ) : null}
                <TouchableOpacity
                  onPress={handleSubmit}
                  style={styles.footerContainer}
                >
                  <Text
                    style={{
                      color: "#ffffff",
                      fontSize: 20,
                    }}
                  >
                    Submit
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </Formik>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Style.GAP,
  },
  textLabel: {
    marginLeft: Style.GAP,
    fontSize: Style.FONT_SIZE_CARD,
    marginTop: Style.GAP,
  },
  textInput: {
    margin: Style.GAP,
    marginTop: 0,
    marginBottom: 8,
    fontSize: Style.FONT_SIZE_CARD,
  },
  footerContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    backgroundColor: "#2A3D6F",
    padding: Style.GAP * 0.6,
    alignItems: "center",
    color: "#fff",
    fontSize: 20,
    borderRadius: 4,
    fontFamily: "AzoSans-Regular",
    margin: Style.GAP,
  },
  error: {
    paddingLeft: Style.GAP,
    color: "#bf0d0d",
    fontSize: Style.FONT_SIZE_BODY,
  },
});
