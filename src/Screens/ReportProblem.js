import * as React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  Linking,
  CameraRoll,
} from "react-native";
import { Headline, Text } from "react-native-paper";
import CardTileReversed from "../Components/CardTileReversed";
import { ScrollView } from "react-native-gesture-handler";
import * as Style from "../Components/styles/Style";

// Load SVG Icons
import LIGHTING_ICON from "../../assets/icons/lightning-electricity-EPS_crop.svg";
import WATER_ICON from "../../assets/icons/water-EPS_crop.svg";
import SEWERAGE_ICON from "../../assets/icons/sewerage_outline_white.svg";
import STREETLIGHT_ICON from "../../assets/icons/streetlight-icon-white.svg";

import Header from "../Components/Header";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

const logoSize = 48;
const hGap = windowWidth * 0.03;

export default function ReportProblem() {
  async function _handleBlockPress() {
    (await Linking.canOpenURL("tel:1800 245 090"))
      ? Linking.openURL("tel:1800 245 090")
      : Linking.openURL(
          "https://www.powerwater.com.au/customers/online-services/contact-us"
        );
  }
  //render the component
  return (
    <>
      <Header title="Report a problem" />
      <ScrollView style={styles.container}>
        <View style={styles.quoteContainer}>
          <Text style={styles.quote}>
            If you see a life-threatening or dangerous situation such as a
            powerline on the ground, call {` `}
            <Text
              style={{ color: Style.COLOR_PRIMARY }}
              onPress={() => Linking.openURL("tel:000")}
            >
              000
            </Text>{" "}
            {` `}
            or the Power and Water emergency line on {` `}
            <Text
              style={{ color: Style.COLOR_PRIMARY }}
              onPress={_handleBlockPress}
            >
              1800 245 090
            </Text>
            {` `}
            immediately.
          </Text>
        </View>
        <Headline style={{ alignSelf: "center" }}>
          Select the type of problem
        </Headline>
        <View style={styles.cardContainer}>
          <View style={styles.column}>
            <CardTileReversed
              title="Power"
              screen="PowerOptionsForm"
              icon={
                <LIGHTING_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={Style.COLOR_WHITE}
                />
              }
              metertype="Report power fault"
            />
            <CardTileReversed
              title="Sewerage"
              screen="SewerageOptionsForm"
              icon={
                <SEWERAGE_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={Style.COLOR_WHITE}
                />
              }
              metertype="Report sewerage fault"
            />
          </View>
          <View style={styles.column}>
            <CardTileReversed
              title="Water"
              icon={
                <WATER_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={Style.COLOR_WHITE}
                />
              }
              screen="WaterOptionsForm"
              metertype="report water fault"
            />
            <CardTileReversed
              title="Streetlight"
              icon={
                <STREETLIGHT_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={Style.COLOR_WHITE}
                />
              }
              screen="StreetlightPreInfo"
              metertype="report streetlight fault"
            />
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <View style={styles.quoteContainer}>
            <Text style={styles.quote}>
              We will process your online report during business hours, 8 am to
              4:30pm, Monday to Friday. To report an outage outside these hours,
              please call {` `}
              <Text
                style={{ color: Style.COLOR_PRIMARY }}
                onPress={_handleBlockPress}
              >
                1800 245 090
              </Text>
              {` `}.
            </Text>
          </View>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    top: 0,
    height: windowHeight,
    width: windowWidth,
    flexDirection: "column",
    backgroundColor: "#fff",
    padding: Style.GAP,
  },
  cardContainer: {
    flex: 3,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "space-around",
  },
  bottomContainer: {
    marginTop: Style.GAP,
  },
  quoteContainer: {
    backgroundColor: "#EAEAEA",
    borderLeftColor: Style.COLOR_PRIMARY,
    padding: hGap,
    borderLeftWidth: 9,
    marginBottom: Style.GAP,
  },
  quote: {
    color: "#333",
    fontSize: Style.FONT_SIZE_BODY,
    textAlign: "auto",
    fontFamily: "AzoSans-Regular",
    height: "auto",
  },
});
