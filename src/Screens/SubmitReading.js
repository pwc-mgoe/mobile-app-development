import * as React from "react";
import { View, StyleSheet, Dimensions, Linking } from "react-native";
import { useTheme, Headline, Text } from "react-native-paper";
import CardTileFlat from "../Components/CardTileFlat";
import { ScrollView } from "react-native-gesture-handler";
import * as Style from "../Components/styles/Style";
// Load SVG Icons
import MECH_ICON from "../../assets/icons/mechanical-meter-white.svg";
import ELEC_ICON from "../../assets/icons/electronic-meter-icon-white.svg";
import TIME_ICON from "../../assets/icons/electronic-meter-icon-white.svg";
import SOLAR_ICON from "../../assets/icons/solar-icon-white.svg";
import WATER_ICON from "../../assets/icons/water-pump-white.svg";

import Header from "../Components/Header";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

const logoSize = 48;

export default function SubmitReading(props) {
  //render the component
  return (
    <>
      <Header title="Submit a meter read" />
      <ScrollView style={styles.container}>
        <Headline style={{ alignSelf: "center" }}>
          Select your meter type
        </Headline>
        <Headline style={styles.headline}>Power</Headline>

        <View style={styles.cardContainer}>
          <Text>
            For help reading your power meter{" "}
            <Text
              style={styles.hyperlink}
              onPress={() =>
                Linking.openURL(
                  "https://www.powerwater.com.au/customers/power/power-meters"
                )
              }
            >
              visit our website
            </Text>
          </Text>
          <CardTileFlat
            title="Standard"
            screen="MeterReading"
            metertype="Standard mechanical meter"
            icon={
              <MECH_ICON
                width={logoSize}
                height={logoSize}
                fill={Style.COLOR_WHITE}
              />
            }
          />
          <CardTileFlat
            title="Electronic"
            screen="MeterReading"
            metertype="Electronic meter"
            icon={
              <ELEC_ICON
                width={logoSize}
                height={logoSize}
                fill={Style.COLOR_WHITE}
              />
            }
          />
          {/*</View>*/}
          {/*<View style={styles.column}>*/}
          <CardTileFlat
            title="Solar"
            screen="DamLevels"
            icon={
              <SOLAR_ICON
                width={logoSize}
                height={logoSize}
                fill={Style.COLOR_WHITE}
              />
            }
            screen="SolarReadingForm"
            metertype="Solar meter"
          />
          {/*</View>*/}
        </View>
        <Headline style={styles.headline}>Water</Headline>

        <View style={styles.cardContainer}>
          <Text>
            For help reading your water meter{" "}
            <Text
              style={styles.hyperlink}
              onPress={() =>
                Linking.openURL(
                  "https://www.powerwater.com.au/customers/water-and-wastewater/my-water-meter"
                )
              }
            >
              visit our website
            </Text>
          </Text>
          <View style={styles.column}>
            <CardTileFlat
              title="Mechanical"
              screen="Outages"
              icon={
                <WATER_ICON
                  width={logoSize}
                  height={logoSize}
                  fill={Style.COLOR_WHITE}
                />
              }
              screen="MeterReading"
              metertype="Water Meter"
            />
          </View>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    top: 0,
    height: windowHeight,
    width: windowWidth,
    flexDirection: "column",
    backgroundColor: "#fff",
    padding: Style.GAP,
  },
  cardContainer: {
    flex: 3,
    // flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "space-around",
  },
  headline: {
    alignSelf: "center",
    fontFamily: Style.FONT_BOLD,
    color: Style.COLOR_PRIMARY,
    paddingTop: Style.GAP,
  },
  bottomContainer: {
    flex: 1,
    flexDirection: "column",
    alignContent: "center",
    justifyContent: "center",
    marginTop: Style.GAP,
    height: windowWidth * 0.2,
  },
  hyperlink: {
    fontFamily: Style.FONT_BOLD,
    color: Style.COLOR_PRIMARY,
  },
});
