import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ImageBackground,
} from "react-native";
import Header from "../Components/Header";
import axios from "axios";
import _ from "underscore";
import { ProgressCircle } from "react-native-svg-charts";

import WEB_ICON from "../../assets/icons/website_crop.svg";
import { ActivityIndicator } from "react-native-paper";

const DAM_BG = require("../../assets/img/darwin-dam-wall.jpg");

const hGap = Dimensions.get("window").width * 0.05;

const DAM_LEVEL_API =
  "http://webservices.powerwater.com.au/WebservicePTP/index.php/damlevels";

export default function DamLevels(props) {
  const mountedRef = React.useRef(true);
  const [damData, setDamData] = React.useState();
  const [isLoading, setIsLoading] = React.useState(true);

  //Get Dam level data
  const fetchDamData = async () => {
    const { data } = await axios(DAM_LEVEL_API);
    setDamData(data.PWCDamLevels.Levels[0].level);
    setIsLoading(false);
  };

  React.useEffect(() => {
    fetchDamData();
    return () => {};
  }, []);

  React.useEffect(() => {
    return () => {
      mountedRef.current = false;
    };
  }, []);

  if (isLoading) {
    return (
      <>
        <Header title="Check dam levels" />
        <ActivityIndicator style={styles.activityIndicator} />
      </>
    );
  }
  return (
    <>
      <Header title="Check dam levels" />
      <View style={{ flex: 1 }}>
        <ImageBackground opacity={0.4} source={DAM_BG} style={styles.imagebg}>
          <Text style={styles.heading}>Darwin River Dam</Text>
          <ProgressCircle
            belowChart={true}
            style={styles.damdata_cirlce}
            progress={damData / 100}
            strokeWidth={30}
            progressColor={"rgb(75,176,228)"}
            backgroundColor="#ffffff"
          >
            <View>
              <Text style={styles.damdata_level}>{damData}%</Text>
            </View>
          </ProgressCircle>
        </ImageBackground>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  activityIndicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  heading: {
    fontSize: 20,
    alignItems: "center",
    textAlign: "center",
    margin: hGap,
    color: "#2A3D6F",
    fontFamily: "AzoSans-Bold",
  },
  imagebg: {
    flex: 1,
    resizeMode: "cover",
  },
  damdata_cirlce: {
    height: 300,
    color: "#fff",
    // backgroundColor: "#ddd",
  },
  damdata_level: {
    fontSize: 72,
    color: "#2A3D6F",
    textAlign: "center",
    fontFamily: "AzoSans-Bold",
    top: 100,
  },
});
