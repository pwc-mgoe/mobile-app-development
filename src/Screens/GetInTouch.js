import React from "react";
import { View, StyleSheet, Dimensions, ScrollView, Image } from "react-native";
import { Text, Headline, List } from "react-native-paper";
import Header from "../Components/Header";
import * as Style from "../Components/styles/Style";

import WEB_ICON from "../../assets/icons/website_crop.svg";
import BlockQuote from "../Components/BlockQuote";
import * as Linking from "expo-linking";

const hGap = Dimensions.get("window").width * 0.05;
const DAM_LEVEL_API =
  "http://webservices.powerwater.com.au/WebservicePTP/index.php/damlevels";

async function openURLorGoToSettings() {
  const emailURL = "mailto:yuba@cloudst.com.au";
  const canOpenEmailApp = await Linking.canOpenURL(emailURL);
  if (canOpenEmailApp) {
    Linking.openURL(emailURL);
  } else {
    Linking.openURL("app-settings:");
  }
}

export default function GetInTouch() {
  return (
    <>
      <Header title="Get in touch" />
      <View style={{ flex: 1, margin: Style.GAP }}>
        <View>
          <List.Item
            title="Contact us online"
            left={(props) => (
              <List.Icon color={Style.COLOR_PRIMARY} icon="account-box" />
            )}
            titleStyle={TitleStyle}
            onPress={async () =>
              await Linking.openURL(
                "https://www.powerwater.com.au/customers/online-services/contact-us"
              )
            }
          />
          <List.Item
            title="Report a fault or emergency"
            description="To report an outage outside business hours, please call our 24-hour line on 1800 245 090."
            left={(props) => (
              <List.Icon color={Style.COLOR_PRIMARY} icon="alert" />
            )}
            descriptionNumberOfLines={4}
            titleStyle={TitleStyle}
            onPress={async () =>
              (await Linking.canOpenURL("tel:1800 245 090"))
                ? Linking.openURL("tel:1800 245 090")
                : Linking.openURL(
                    "https://www.powerwater.com.au/customers/online-services/contact-us"
                  )
            }
          />
          <List.Item
            title="General enquiries"
            description="1800 245 092 (8am to 5pm Mon-Fri)"
            left={(props) => (
              <List.Icon color={Style.COLOR_PRIMARY} icon="phone" />
            )}
            titleStyle={TitleStyle}
            onPress={async () =>
              (await Linking.canOpenURL("tel:1800 245 092"))
                ? Linking.openURL("tel:1800 245 092")
                : Linking.openURL(
                    "https://www.powerwater.com.au/customers/online-services/contact-us"
                  )
            }
          />
          <List.Item
            title="Visit our website"
            left={(props) => (
              <List.Icon icon="web" color={Style.COLOR_PRIMARY} />
            )}
            titleStyle={TitleStyle}
            onPress={() => Linking.openURL("https://www.powerwater.com.au/")}
          />
        </View>
      </View>
    </>
  );
}

const TitleStyle = {
  color: Style.COLOR_PRIMARY,
  fontFamily: Style.FONT_BOLD,
  fontSize: Style.FONT_SIZE_CARD,
};
const styles = StyleSheet.create({
  Headline: {
    alignItems: "center",
    textAlign: "auto",
    padding: hGap,
    paddingBottom: 0,
    color: Style.COLOR_PRIMARY,
    fontFamily: Style.FONT_BOLD,
  },
});
