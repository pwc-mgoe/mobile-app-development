import React from "react";
import { View, StyleSheet, Dimensions, ScrollView } from "react-native";
import { Text, Button, List, Headline, Avatar } from "react-native-paper";
import Header from "../Components/Header";
import * as Style from "../Components/styles/Style";
import _ from "underscore";
import { useNavigation } from "@react-navigation/native";

export default function ThankYou(props) {
  const { route } = props;
  const { msg } = route.params;

  const navigation = useNavigation();

  const goHome = () => {
    navigation.navigate("Home");
  };
  return (
    <>
      {/* <Header title="Get in touch" /> */}
      <View
        style={{
          flex: 1,
          margin: Style.GAP,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Avatar.Image
          size={48}
          source={require("../../assets/icons/tick.gif")}
        />
        <Headline
          style={{ textAlign: "center", margin: Style.GAP, padding: Style.GAP }}
        >
          {msg}
        </Headline>
        <Button mode="contained" icon="home" onPress={goHome} uppercase={false}>
          Return to home screen
        </Button>
      </View>
    </>
  );
}
