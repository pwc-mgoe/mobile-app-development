import React, { useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";

import HomeScreen from "./Screens/HomeScreen";
import Outages from "./Screens/Outages";
import MakePayment from "./Screens/MakePayment";
import DamLevels from "./Screens/DamLevels";
import ReportProblem from "./Screens/ReportProblem";
import SubmitReading from "./Screens/SubmitReading";
import Payment from "./Screens/Payment";
import GetInTouch from "./Screens/GetInTouch";
import MeterReading from "./Components/Forms/MeterReading/MeterReading";
import ThankYou from "./Screens/ThankYou";
import About from "./Screens/About";
import ReportProblemForm from "./Components/Forms/ReportProblem/ReportProblemForm";
import TimeOfUseReadingForm from "./Components/Forms/MeterReading/TimeOfUseReadingForm";
import SolarReadingForm from "./Components/Forms/MeterReading/SolarReadingForm";
import PowerOptions from "./Components/Forms/ReportProblem/PowerOptions";
import WaterOptions from "./Components/Forms/ReportProblem/WaterOptions";
import SewerageOptions from "./Components/Forms/ReportProblem/SewerageOptions";
import StreetlightOptions from "./Components/Forms/ReportProblem/StreetlightOptions";
import StreetlightPreInfo from "./Components/Forms/ReportProblem/StreetlightPreInfo";
//stack navigator
const Stack = createStackNavigator();

export default function Index(props) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        headerBackStyle: {
          color: "#fff",
        },
        headerTitleStyle: {
          color: "#fff",
          fontStyle: "normal",
          fontSize: 20,
          justifyContent: "center",
        },
        headerStyle: {
          backgroundColor: "#2a3d6f",
          // height: Dimensions.get("screen").height * 0.15
          borderBottomColor: "transparent",
        },
        headerBackTitle: "he",
        headerLeftContainerStyle: {
          margin: 4,
        },
      }}
      initialRouteName="Home"
    >
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ title: "Home", headerShown: false }}
      />
      <Stack.Screen
        name="Outages"
        component={Outages}
        options={{
          title: "Current outages",
        }}
      />
      <Stack.Screen
        name="MakePayment"
        component={MakePayment}
        options={{ title: "Make Payments" }}
      />
      <Stack.Screen
        name="DamLevels"
        component={DamLevels}
        options={{ title: "Current Dam levels" }}
      />
      <Stack.Screen
        name="Payment"
        component={Payment}
        options={{ title: "Payment" }}
      />
      <Stack.Screen
        name="GetInTouch"
        component={GetInTouch}
        options={{ title: "Get In Touch" }}
      />
      <Stack.Screen
        name="ReportProblem"
        component={ReportProblem}
        options={{ title: "Report Problem" }}
      />
      <Stack.Screen
        name="SubmitReading"
        component={SubmitReading}
        options={{ title: "Submit meter reading" }}
      />
      <Stack.Screen
        name="MeterReading"
        component={MeterReading}
        options={{ title: "Meter reading Form" }}
      />
      <Stack.Screen
        name="TimeOfUseForm"
        component={TimeOfUseReadingForm}
        options={{ title: "Time of Use reading form" }}
      />
      <Stack.Screen
        name="SolarReadingForm"
        component={SolarReadingForm}
        options={{ title: "Time of Use reading form" }}
      />
      <Stack.Screen
        name="ReportProblemForm"
        component={ReportProblemForm}
        options={{ title: "Report Problem Form" }}
      />
      <Stack.Screen
        name="PowerOptionsForm"
        component={PowerOptions}
        options={{ title: "Report Power Problem Form" }}
      />

      <Stack.Screen
        name="WaterOptionsForm"
        component={WaterOptions}
        options={{ title: "Report Water Problem Form" }}
      />
      <Stack.Screen
        name="SewerageOptionsForm"
        component={SewerageOptions}
        options={{ title: "Report sewerage problem Form" }}
      />
      <Stack.Screen
        name="StreetlightPreInfo"
        component={StreetlightPreInfo}
        options={{ title: "Report streetlight Problem Form" }}
      />
      <Stack.Screen
        name="StreetlightOptionsForm"
        component={StreetlightOptions}
        options={{ title: "Report streetlight Problem Form" }}
      />
      <Stack.Screen
        name="ThankYou"
        component={ThankYou}
        options={{ title: "Thank You!" }}
      />
      <Stack.Screen
        name="About"
        component={About}
        options={{ title: "About us" }}
      />
    </Stack.Navigator>
  );
}
